﻿using API.Models;
using AutoMapper;
using BL.Models;
using DAL.Models;

namespace API.Helpers
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<User, UserDTO>()
                .ReverseMap();
            CreateMap<Project, ProjectDTO>()
                .ReverseMap();
            CreateMap<Team, TeamDTO>()
                .ReverseMap();
            CreateMap<Tasks, TasksDTO>()
                .ReverseMap();
            CreateMap<ProjectInfo, ProjectInfoDTO>();
            CreateMap<UserInfo, UserInfoDTO>();
            CreateMap<UserTasks, UserTasksDTO>();
        }
    }
}
