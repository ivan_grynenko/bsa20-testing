﻿using DAL.Models;
using System;

namespace API.Models
{
    public class ProjectInfoDTO
    {
        public Project Project { get; set; }
        public Tasks LongestTask { get; set; }
        public Tasks ShortestTask { get; set; }
    }
}
