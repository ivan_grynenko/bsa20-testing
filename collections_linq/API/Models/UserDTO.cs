﻿using System;
using System.ComponentModel.DataAnnotations;

namespace API.Models
{
    public class UserDTO
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime Birthday { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime RegisteredAt { get; set; }
        public int? TeamId { get; set; }
    }
}
