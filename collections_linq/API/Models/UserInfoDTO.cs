﻿using DAL.Models;
using System;

namespace API.Models
{
    public class UserInfoDTO
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int? OverallNumOfTasks { get; set; }
        public int? OverallNumOfCanceledTasks { get; set; }
        public Tasks LongestTask { get; set; }
    }
}
