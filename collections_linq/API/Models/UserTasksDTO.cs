﻿using DAL.Models;
using System;
using System.Collections.Generic;

namespace API.Models
{
    public class UserTasksDTO
    {
        public User User { get; set; }
        public IEnumerable<Tasks> Tasks { get; set; }
    }
}
