﻿using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Base
{
    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : BaseModel
    {
        private readonly ApplicationContext context;
        private DbSet<TEntity> entities;

        public virtual DbSet<TEntity> Entities { get => entities; }

        public Repository(ApplicationContext context)
        {
            this.context = context;
            entities = context.Set<TEntity>();
        }

        public virtual async Task Add(IEnumerable<TEntity> entities)
        {
            await this.entities.AddRangeAsync(entities);
        }

        public virtual async Task Add(TEntity entity)
        {
            await entities.AddAsync(entity);
        }

        public virtual async Task<IEnumerable<TEntity>> GetAll()
        {
            return await entities.ToListAsync();
        }

        public virtual async Task<TEntity> GetById(int id)
        {
            return await entities.FirstOrDefaultAsync(e => e.Id == id);
        }

        public virtual void Remove(IEnumerable<TEntity> entities)
        {
            this.entities.RemoveRange(entities);
        }

        public virtual void Remove(TEntity entity)
        {
            entities.Remove(entity);
        }

        public virtual void Update(IEnumerable<TEntity> entities)
        {
            this.entities.UpdateRange(entities);
        }

        public virtual void Update(TEntity entity)
        {
            entities.Update(entity);
        }

        public void Dispose()
        {
            entities = null;
        }
    }
}
