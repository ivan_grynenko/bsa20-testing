﻿using DAL.Base;
using DAL.Interfaces;
using DAL.Models;

namespace DAL.Repositories
{
    public class ProjectRepository : Repository<Project>, IProjectRepository
    {
        public ProjectRepository(ApplicationContext context)
            : base(context)
        { }
    }
}
