﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public class Project : BaseModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? Deadline { get; set; }
        public int AuthorId { get; set; }
        public int? TeamId { get; set; }
        public Team Team { get; set; }
        public List<Tasks> Tasks { get; set; }
    }
}
