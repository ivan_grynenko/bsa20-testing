﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public class Team : BaseModel
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public List<User> Users { get; set; }
        public List<Project> Projects { get; set; }
    }
}
