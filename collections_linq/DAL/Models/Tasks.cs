﻿using System;

namespace DAL.Models
{
    public class Tasks : BaseModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
        public int StateId { get; set; }
        public int? ProjectId { get; set; }
        public int PerformerId { get; set; }
        public User Performer { get; set; }
        public Project Project { get; set; }
        public TaskState State { get; set; }
    }
}
