﻿using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Linq;

namespace DAL.Configuration
{
    public class TaskStatesConfiguration : IEntityTypeConfiguration<TaskState>
    {
        public void Configure(EntityTypeBuilder<TaskState> builder)
        {
            builder.ToTable("TaskStates");
            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(50);

            var id = 1;
            var basicData = new string[] { "Created", "Started", "Finished", "Canceled" }
            .Select(e => new TaskState() { Id = id++, Name = e });

            builder.HasData(basicData);
        }
    }
}
