﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class AddingSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "TaskStates",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Created" },
                    { 2, "Started" },
                    { 3, "Finished" },
                    { 4, "Canceled" }
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2016, 10, 6, 16, 51, 12, 275, DateTimeKind.Unspecified).AddTicks(5323), "Team A" },
                    { 2, new DateTime(2015, 12, 17, 7, 4, 31, 490, DateTimeKind.Unspecified).AddTicks(2486), "Team B" },
                    { 3, new DateTime(2017, 11, 4, 18, 23, 58, 123, DateTimeKind.Unspecified).AddTicks(7930), "Team C" },
                    { 4, new DateTime(2018, 3, 19, 5, 33, 58, 0, DateTimeKind.Unspecified).AddTicks(3675), "Team D" },
                    { 5, new DateTime(2015, 12, 18, 9, 28, 14, 545, DateTimeKind.Unspecified).AddTicks(3792), "Team E" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 26, new DateTime(1997, 10, 26, 18, 39, 23, 475, DateTimeKind.Unspecified).AddTicks(3228), "Francesca_Pouros92@gmail.com", "Francesca", "Pouros", new DateTime(2011, 3, 12, 16, 43, 32, 341, DateTimeKind.Unspecified).AddTicks(882), null },
                    { 24, new DateTime(1983, 8, 15, 14, 21, 13, 505, DateTimeKind.Unspecified).AddTicks(8637), "Eriberto.Collins@gmail.com", "Eriberto", "Collins", new DateTime(2012, 8, 15, 13, 31, 41, 666, DateTimeKind.Unspecified).AddTicks(3237), null },
                    { 23, new DateTime(1997, 5, 16, 3, 14, 32, 305, DateTimeKind.Unspecified).AddTicks(306), "Gia53@gmail.com", "Gia", "Schoen", new DateTime(2011, 12, 10, 23, 24, 40, 268, DateTimeKind.Unspecified).AddTicks(633), null },
                    { 22, new DateTime(1973, 1, 15, 2, 42, 45, 219, DateTimeKind.Unspecified).AddTicks(2564), "Dexter.Adams@yahoo.com", "Dexter", "Adams", new DateTime(2015, 4, 10, 8, 55, 2, 820, DateTimeKind.Unspecified).AddTicks(5887), null },
                    { 21, new DateTime(2008, 8, 31, 9, 35, 51, 705, DateTimeKind.Unspecified).AddTicks(5046), "Jairo_Donnelly@gmail.com", "Jairo", "Donnelly", new DateTime(2018, 8, 18, 3, 54, 40, 470, DateTimeKind.Unspecified).AddTicks(4211), null },
                    { 20, new DateTime(1969, 4, 29, 16, 57, 40, 302, DateTimeKind.Unspecified).AddTicks(8314), "Colton54@gmail.com", "Colton", "Greenfelder", new DateTime(2015, 4, 12, 6, 12, 20, 104, DateTimeKind.Unspecified).AddTicks(616), null },
                    { 17, new DateTime(1986, 2, 22, 13, 30, 44, 346, DateTimeKind.Unspecified).AddTicks(2315), "Austyn.Hilpert18@yahoo.com", "Austyn", "Hilpert", new DateTime(2010, 7, 28, 22, 27, 42, 32, DateTimeKind.Unspecified).AddTicks(1644), null },
                    { 10, new DateTime(1996, 7, 6, 2, 34, 34, 745, DateTimeKind.Unspecified).AddTicks(4732), "Herman_Mertz@gmail.com", "Herman", "Mertz", new DateTime(2019, 3, 10, 15, 53, 35, 330, DateTimeKind.Unspecified).AddTicks(5284), null },
                    { 15, new DateTime(1993, 3, 16, 15, 4, 7, 873, DateTimeKind.Unspecified).AddTicks(5952), "Saul.Jast72@gmail.com", "Saul", "Jast", new DateTime(2017, 8, 27, 11, 48, 33, 160, DateTimeKind.Unspecified).AddTicks(7698), null },
                    { 14, new DateTime(2002, 5, 2, 12, 1, 18, 288, DateTimeKind.Unspecified).AddTicks(686), "Marian39@hotmail.com", "Marian", "Muller", new DateTime(2012, 2, 27, 9, 29, 26, 451, DateTimeKind.Unspecified).AddTicks(3983), null },
                    { 13, new DateTime(1972, 12, 9, 6, 19, 47, 928, DateTimeKind.Unspecified).AddTicks(7111), "Norma39@gmail.com", "Norma", "Steuber", new DateTime(2019, 7, 13, 20, 18, 9, 971, DateTimeKind.Unspecified).AddTicks(2299), null },
                    { 27, new DateTime(2008, 2, 8, 16, 42, 46, 929, DateTimeKind.Unspecified).AddTicks(8452), "Darlene_Olson@yahoo.com", "Darlene", "Olson", new DateTime(2015, 3, 15, 6, 0, 11, 332, DateTimeKind.Unspecified).AddTicks(1225), null },
                    { 6, new DateTime(1970, 9, 24, 22, 10, 36, 993, DateTimeKind.Unspecified).AddTicks(3098), "Vladimir81@gmail.com", "Vladimir", "Reichel", new DateTime(2016, 5, 13, 15, 47, 38, 293, DateTimeKind.Unspecified).AddTicks(543), null },
                    { 5, new DateTime(1990, 8, 2, 12, 4, 59, 346, DateTimeKind.Unspecified).AddTicks(6162), "Tierra45@hotmail.com", "Tierra", "Stracke", new DateTime(2013, 7, 21, 10, 48, 29, 154, DateTimeKind.Unspecified).AddTicks(9377), null },
                    { 3, new DateTime(2008, 8, 19, 2, 55, 28, 821, DateTimeKind.Unspecified).AddTicks(7132), "Fred_Marks21@yahoo.com", "Fred", "Marks", new DateTime(2013, 4, 6, 14, 47, 27, 496, DateTimeKind.Unspecified).AddTicks(1604), null },
                    { 16, new DateTime(1965, 5, 14, 18, 30, 9, 86, DateTimeKind.Unspecified).AddTicks(5773), "Wilber_Douglas0@gmail.com", "Wilber", "Douglas", new DateTime(2017, 9, 20, 8, 4, 30, 463, DateTimeKind.Unspecified).AddTicks(3902), null },
                    { 28, new DateTime(2009, 5, 11, 16, 47, 27, 580, DateTimeKind.Unspecified).AddTicks(7470), "Marley57@gmail.com", "Marley", "Orn", new DateTime(2011, 2, 14, 23, 38, 51, 865, DateTimeKind.Unspecified).AddTicks(7741), null }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 2, 27, new DateTime(2017, 11, 15, 16, 50, 50, 328, DateTimeKind.Unspecified).AddTicks(6316), new DateTime(2020, 4, 10, 7, 17, 34, 221, DateTimeKind.Unspecified).AddTicks(1968), "Aut cum repudiandae et nam facilis natus porro est eveniet soluta distinctio eaque quis.", "Intelligent Metal Mouse", 1 },
                    { 9, 10, new DateTime(2019, 6, 26, 14, 59, 8, 557, DateTimeKind.Unspecified).AddTicks(2545), new DateTime(2020, 1, 6, 8, 35, 50, 303, DateTimeKind.Unspecified).AddTicks(4319), "Impedit quod veniam dolores fugit voluptates.", "back-end", 1 },
                    { 3, 18, new DateTime(2018, 12, 26, 9, 36, 59, 567, DateTimeKind.Unspecified).AddTicks(2971), new DateTime(2021, 2, 1, 3, 27, 51, 150, DateTimeKind.Unspecified).AddTicks(9494), "Dolorem nam laboriosam excepturi sed asperiores harum alias.", "Plastic", 2 },
                    { 10, 4, new DateTime(2018, 9, 26, 11, 37, 6, 109, DateTimeKind.Unspecified).AddTicks(608), new DateTime(2020, 8, 21, 8, 2, 31, 718, DateTimeKind.Unspecified).AddTicks(131), "Id ut voluptatem debitis qui quibusdam mollitia ad sit beatae sint deserunt culpa.", "Senior", 5 },
                    { 4, 30, new DateTime(2018, 6, 6, 15, 8, 15, 941, DateTimeKind.Unspecified).AddTicks(5317), new DateTime(2021, 3, 19, 18, 0, 5, 648, DateTimeKind.Unspecified).AddTicks(1186), "Voluptas ut voluptas totam aut ut et eos placeat.", "attitude-oriented", 3 },
                    { 6, 5, new DateTime(2019, 9, 28, 21, 6, 3, 407, DateTimeKind.Unspecified).AddTicks(1570), new DateTime(2021, 9, 21, 23, 16, 38, 84, DateTimeKind.Unspecified).AddTicks(9626), "Ab provident reprehenderit quis tempore eum odit architecto est.", "orchid", 3 },
                    { 8, 26, new DateTime(2018, 6, 25, 5, 51, 9, 25, DateTimeKind.Unspecified).AddTicks(1389), new DateTime(2021, 12, 13, 14, 42, 25, 241, DateTimeKind.Unspecified).AddTicks(9155), "Doloremque sit architecto magnam dolorem omnis qui reiciendis cumque molestiae ea corporis voluptatum.", "Security", 3 },
                    { 7, 26, new DateTime(2017, 11, 26, 19, 31, 56, 318, DateTimeKind.Unspecified).AddTicks(3719), new DateTime(2020, 7, 7, 1, 46, 12, 593, DateTimeKind.Unspecified).AddTicks(8022), "Dignissimos id rem eveniet exercitationem aut itaque dolorum error commodi voluptas.", "International", 5 },
                    { 5, 17, new DateTime(2018, 8, 31, 3, 46, 14, 437, DateTimeKind.Unspecified).AddTicks(9852), new DateTime(2020, 2, 11, 7, 12, 59, 630, DateTimeKind.Unspecified).AddTicks(9741), "Molestiae quasi ea consequatur quas quae eius laboriosam in cumque tempore aut.", "Avon", 4 },
                    { 1, 2, new DateTime(2017, 6, 23, 7, 41, 42, 380, DateTimeKind.Unspecified).AddTicks(3914), new DateTime(2021, 9, 13, 7, 46, 12, 193, DateTimeKind.Unspecified).AddTicks(2512), "Vero rem quis quis dolores neque.", "expedite", 5 }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 7, new DateTime(1982, 5, 29, 15, 6, 26, 598, DateTimeKind.Unspecified).AddTicks(2905), "Bailey.Dooley@hotmail.com", "Bailey", "Dooley", new DateTime(2013, 8, 30, 23, 45, 27, 881, DateTimeKind.Unspecified).AddTicks(5346), 5 },
                    { 2, new DateTime(1966, 1, 9, 10, 15, 50, 984, DateTimeKind.Unspecified).AddTicks(1033), "Oscar_Nikolaus25@hotmail.com", "Oscar", "Nikolaus", new DateTime(2012, 8, 3, 1, 44, 17, 903, DateTimeKind.Unspecified).AddTicks(9286), 5 },
                    { 30, new DateTime(1961, 5, 2, 18, 33, 28, 369, DateTimeKind.Unspecified).AddTicks(4635), "Janis.Legros32@gmail.com", "Janis", "Legros", new DateTime(2013, 3, 12, 14, 46, 30, 255, DateTimeKind.Unspecified).AddTicks(3839), 4 },
                    { 19, new DateTime(1963, 11, 9, 23, 20, 37, 455, DateTimeKind.Unspecified).AddTicks(9838), "Jean_OReilly@hotmail.com", "Jean", "O'Reilly", new DateTime(2016, 11, 5, 17, 35, 27, 396, DateTimeKind.Unspecified).AddTicks(3466), 4 },
                    { 1, new DateTime(1997, 5, 29, 5, 54, 25, 934, DateTimeKind.Unspecified).AddTicks(5518), "Columbus_Walter@hotmail.com", "Columbus", "Walter", new DateTime(2011, 11, 16, 10, 2, 54, 240, DateTimeKind.Unspecified).AddTicks(4290), 4 },
                    { 4, new DateTime(2009, 5, 19, 18, 14, 46, 831, DateTimeKind.Unspecified).AddTicks(682), "Jess.Upton57@hotmail.com", "Jess", "Upton", new DateTime(2017, 3, 30, 6, 24, 48, 297, DateTimeKind.Unspecified).AddTicks(137), 4 },
                    { 9, new DateTime(1986, 4, 26, 10, 19, 52, 599, DateTimeKind.Unspecified).AddTicks(2885), "Nova87@yahoo.com", "Nova", "Carter", new DateTime(2010, 6, 30, 19, 16, 33, 575, DateTimeKind.Unspecified).AddTicks(1551), 5 },
                    { 18, new DateTime(1984, 12, 3, 7, 2, 11, 412, DateTimeKind.Unspecified).AddTicks(798), "Ida_Flatley42@hotmail.com", "Ida", "Flatley", new DateTime(2020, 7, 6, 5, 47, 54, 532, DateTimeKind.Unspecified).AddTicks(6376), 3 },
                    { 29, new DateTime(2009, 1, 2, 6, 35, 54, 522, DateTimeKind.Unspecified).AddTicks(2616), "Gaston.Wolff@hotmail.com", "Gaston", "Wolff", new DateTime(2018, 9, 3, 16, 32, 53, 37, DateTimeKind.Unspecified).AddTicks(2080), 2 },
                    { 25, new DateTime(1970, 7, 21, 2, 45, 20, 299, DateTimeKind.Unspecified).AddTicks(5660), "Glenda82@gmail.com", "Glenda", "Terry", new DateTime(2016, 5, 26, 20, 23, 16, 348, DateTimeKind.Unspecified).AddTicks(6290), 2 },
                    { 11, new DateTime(1989, 4, 2, 17, 30, 50, 875, DateTimeKind.Unspecified).AddTicks(5396), "Devonte_Kuhlman@yahoo.com", "Devonte", "Kuhlman", new DateTime(2019, 2, 2, 6, 47, 44, 261, DateTimeKind.Unspecified).AddTicks(7580), 1 },
                    { 8, new DateTime(1991, 4, 12, 22, 40, 13, 106, DateTimeKind.Unspecified).AddTicks(2678), "Clair43@hotmail.com", "Clair", "McDermott", new DateTime(2019, 9, 19, 12, 54, 2, 386, DateTimeKind.Unspecified).AddTicks(2498), 4 },
                    { 12, new DateTime(1962, 4, 28, 13, 52, 1, 616, DateTimeKind.Unspecified).AddTicks(1096), "Emmitt.Price@gmail.com", "Emmitt", "Price", new DateTime(2017, 7, 9, 18, 22, 27, 492, DateTimeKind.Unspecified).AddTicks(9201), 5 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[,]
                {
                    { 26, new DateTime(2018, 1, 9, 0, 40, 40, 498, DateTimeKind.Unspecified).AddTicks(8941), "Quis distinctio reprehenderit molestias qui veritatis et sint.", new DateTime(2019, 4, 27, 14, 26, 23, 171, DateTimeKind.Unspecified).AddTicks(781), "intuitive", 27, 9, 2 },
                    { 4, new DateTime(2017, 1, 3, 21, 24, 13, 29, DateTimeKind.Unspecified).AddTicks(5314), "Qui commodi harum aliquam numquam quos provident.", new DateTime(2019, 3, 23, 8, 12, 6, 861, DateTimeKind.Unspecified).AddTicks(110), "Home Loan Account", 1, 8, 2 },
                    { 27, new DateTime(2018, 4, 18, 1, 37, 44, 87, DateTimeKind.Unspecified).AddTicks(5277), "Aut voluptatem inventore sapiente ipsa animi sunt.", new DateTime(2019, 11, 5, 12, 22, 46, 5, DateTimeKind.Unspecified).AddTicks(7067), "Small Plastic Bacon", 1, 3, 4 },
                    { 37, new DateTime(2018, 9, 7, 8, 28, 48, 122, DateTimeKind.Unspecified).AddTicks(5794), "Omnis debitis ex molestiae qui illo accusamus est.", new DateTime(2017, 10, 25, 11, 45, 55, 435, DateTimeKind.Unspecified).AddTicks(8960), "Consultant", 1, 8, 3 },
                    { 8, new DateTime(2017, 12, 1, 19, 38, 44, 627, DateTimeKind.Unspecified).AddTicks(5635), "Voluptatum fugit ea quo occaecati quos non.", new DateTime(2017, 10, 16, 9, 7, 29, 575, DateTimeKind.Unspecified).AddTicks(5861), "contingency", 4, 2, 1 },
                    { 23, new DateTime(2017, 12, 22, 6, 11, 29, 351, DateTimeKind.Unspecified).AddTicks(4513), "Occaecati officia omnis eos ea dolores in sunt.", new DateTime(2017, 12, 9, 8, 42, 52, 680, DateTimeKind.Unspecified).AddTicks(4356), "Human", 4, 2, 1 },
                    { 47, new DateTime(2017, 2, 13, 1, 18, 39, 774, DateTimeKind.Unspecified).AddTicks(4019), "Sit placeat aut exercitationem et molestiae.", new DateTime(2018, 5, 30, 8, 1, 34, 985, DateTimeKind.Unspecified).AddTicks(906), "Platinum", 19, 6, 2 },
                    { 48, new DateTime(2018, 9, 4, 17, 8, 33, 523, DateTimeKind.Unspecified).AddTicks(3883), "Quaerat et reiciendis et molestiae doloremque fugiat ut illum distinctio.", new DateTime(2017, 2, 25, 5, 0, 7, 417, DateTimeKind.Unspecified).AddTicks(2097), "Pike", 19, 9, 4 },
                    { 10, new DateTime(2018, 5, 13, 15, 40, 1, 862, DateTimeKind.Unspecified).AddTicks(7176), "Vitae quasi quia a sed optio maiores nulla.", new DateTime(2019, 8, 20, 12, 38, 31, 354, DateTimeKind.Unspecified).AddTicks(6024), "Steel", 24, 1, 1 },
                    { 14, new DateTime(2018, 7, 5, 12, 29, 31, 787, DateTimeKind.Unspecified).AddTicks(713), "Aut animi sed provident ut voluptate et id fugiat ullam ad.", new DateTime(2018, 1, 17, 17, 26, 42, 553, DateTimeKind.Unspecified).AddTicks(8832), "B2B", 11, 1, 4 },
                    { 29, new DateTime(2018, 8, 6, 22, 14, 39, 636, DateTimeKind.Unspecified).AddTicks(7333), "Itaque eaque vel qui id quo nostrum et inventore.", new DateTime(2019, 12, 26, 22, 25, 52, 279, DateTimeKind.Unspecified).AddTicks(8144), "Gorgeous Concrete Car", 10, 1, 4 },
                    { 42, new DateTime(2017, 11, 6, 23, 29, 26, 406, DateTimeKind.Unspecified).AddTicks(6455), "Aspernatur minima voluptatem vel accusantium.", new DateTime(2019, 1, 6, 0, 57, 22, 11, DateTimeKind.Unspecified).AddTicks(1545), "Handcrafted", 8, 1, 2 },
                    { 45, new DateTime(2017, 2, 14, 9, 6, 42, 518, DateTimeKind.Unspecified).AddTicks(6125), "Reiciendis sint cupiditate quasi natus sit libero sit quo.", new DateTime(2018, 9, 6, 12, 37, 56, 938, DateTimeKind.Unspecified).AddTicks(3048), "hacking", 15, 1, 2 },
                    { 28, new DateTime(2018, 11, 8, 14, 48, 42, 69, DateTimeKind.Unspecified).AddTicks(6042), "Et sunt quisquam ratione rerum iste ut maxime.", new DateTime(2017, 4, 29, 9, 16, 20, 792, DateTimeKind.Unspecified).AddTicks(1407), "FTP", 18, 7, 3 },
                    { 36, new DateTime(2018, 1, 30, 13, 51, 44, 378, DateTimeKind.Unspecified).AddTicks(3837), "Sunt consectetur cumque eaque ut vero consectetur quia adipisci.", new DateTime(2018, 7, 11, 18, 46, 34, 902, DateTimeKind.Unspecified).AddTicks(8862), "Keys", 1, 7, 1 },
                    { 44, new DateTime(2018, 7, 9, 7, 50, 37, 232, DateTimeKind.Unspecified).AddTicks(9724), "Exercitationem quaerat est praesentium beatae dolorem nobis omnis.", new DateTime(2019, 5, 30, 4, 15, 18, 111, DateTimeKind.Unspecified).AddTicks(5860), "standardization", 25, 7, 3 },
                    { 16, new DateTime(2017, 1, 11, 17, 12, 36, 210, DateTimeKind.Unspecified).AddTicks(7464), "Ut pariatur rerum impedit reiciendis quibusdam et iusto.", new DateTime(2020, 1, 3, 11, 5, 14, 662, DateTimeKind.Unspecified).AddTicks(6189), "Plains", 14, 10, 1 },
                    { 30, new DateTime(2017, 3, 28, 17, 33, 18, 269, DateTimeKind.Unspecified).AddTicks(1076), "Veritatis blanditiis praesentium error libero.", new DateTime(2019, 7, 15, 11, 49, 5, 440, DateTimeKind.Unspecified).AddTicks(8456), "deposit", 17, 10, 4 },
                    { 5, new DateTime(2018, 10, 11, 5, 2, 33, 449, DateTimeKind.Unspecified).AddTicks(789), "Placeat animi voluptate autem ipsam explicabo aut.", new DateTime(2018, 9, 1, 16, 27, 10, 695, DateTimeKind.Unspecified).AddTicks(1613), "Extended", 2, 7, 2 },
                    { 24, new DateTime(2017, 7, 13, 10, 2, 15, 76, DateTimeKind.Unspecified).AddTicks(1567), "Dolorem perferendis rerum voluptas excepturi cumque perferendis eos quos.", new DateTime(2020, 2, 21, 1, 57, 55, 406, DateTimeKind.Unspecified).AddTicks(4501), "Optional", 7, 4, 1 },
                    { 35, new DateTime(2018, 10, 10, 8, 30, 22, 868, DateTimeKind.Unspecified).AddTicks(3540), "Illum maxime officia doloremque dolores ut consequatur occaecati nisi voluptas impedit aut blanditiis.", new DateTime(2017, 4, 6, 1, 43, 17, 188, DateTimeKind.Unspecified).AddTicks(1604), "interfaces", 7, 7, 2 },
                    { 17, new DateTime(2017, 1, 12, 10, 52, 16, 901, DateTimeKind.Unspecified).AddTicks(5176), "Voluptatibus temporibus laborum aut quia aut dolorum deserunt rerum eius deserunt rem.", new DateTime(2018, 11, 18, 9, 17, 54, 847, DateTimeKind.Unspecified).AddTicks(767), "Division", 9, 7, 3 },
                    { 33, new DateTime(2018, 12, 8, 20, 27, 53, 331, DateTimeKind.Unspecified).AddTicks(5870), "Aut ratione ea quidem laborum in est dolores omnis ullam voluptatibus.", new DateTime(2017, 8, 8, 1, 40, 23, 449, DateTimeKind.Unspecified).AddTicks(7096), "Buckinghamshire", 10, 5, 4 },
                    { 25, new DateTime(2018, 11, 2, 22, 33, 56, 990, DateTimeKind.Unspecified).AddTicks(9069), "Et perspiciatis rerum velit adipisci perferendis tenetur cum quibusdam repudiandae consequuntur ut suscipit laborum.", new DateTime(2018, 2, 5, 15, 40, 3, 994, DateTimeKind.Unspecified).AddTicks(431), "Producer", 16, 5, 4 },
                    { 21, new DateTime(2018, 10, 23, 21, 51, 54, 813, DateTimeKind.Unspecified).AddTicks(4777), "Aut omnis vel officiis iste porro ipsa.", new DateTime(2019, 10, 13, 8, 29, 42, 415, DateTimeKind.Unspecified).AddTicks(6887), "array", 24, 5, 3 },
                    { 13, new DateTime(2018, 7, 11, 4, 11, 26, 872, DateTimeKind.Unspecified).AddTicks(8320), "Distinctio voluptas facere quae non culpa harum eius impedit temporibus qui pariatur.", new DateTime(2019, 11, 7, 13, 26, 23, 883, DateTimeKind.Unspecified).AddTicks(5018), "Generic", 16, 5, 3 },
                    { 41, new DateTime(2018, 8, 8, 17, 2, 17, 260, DateTimeKind.Unspecified).AddTicks(6533), "Ipsam nemo rerum vel temporibus.", new DateTime(2020, 7, 13, 1, 12, 16, 594, DateTimeKind.Unspecified).AddTicks(280), "Borders", 28, 9, 4 },
                    { 9, new DateTime(2017, 8, 28, 18, 59, 33, 576, DateTimeKind.Unspecified).AddTicks(7459), "Velit vel omnis et natus ut dolorem explicabo exercitationem consequatur et.", new DateTime(2020, 7, 16, 8, 28, 13, 244, DateTimeKind.Unspecified).AddTicks(8672), "SMS", 21, 3, 4 },
                    { 11, new DateTime(2017, 9, 30, 8, 34, 5, 496, DateTimeKind.Unspecified).AddTicks(5974), "Incidunt ex sit natus quam unde et ea.", new DateTime(2019, 11, 22, 22, 27, 30, 235, DateTimeKind.Unspecified).AddTicks(2133), "Buckinghamshire", 17, 3, 3 },
                    { 22, new DateTime(2017, 9, 11, 13, 59, 48, 65, DateTimeKind.Unspecified).AddTicks(4406), "Optio pariatur animi eos dolores non nihil.", new DateTime(2017, 3, 7, 23, 18, 51, 333, DateTimeKind.Unspecified).AddTicks(8210), "California", 3, 3, 2 },
                    { 32, new DateTime(2018, 9, 10, 9, 43, 52, 747, DateTimeKind.Unspecified).AddTicks(719), "Velit id vero deleniti magni illum quia quia debitis ut.", new DateTime(2018, 9, 26, 20, 29, 59, 189, DateTimeKind.Unspecified).AddTicks(6190), "Licensed", 17, 3, 2 },
                    { 43, new DateTime(2018, 11, 14, 0, 17, 25, 667, DateTimeKind.Unspecified).AddTicks(8753), "Ab quia quos veniam sit dolor accusantium.", new DateTime(2018, 9, 19, 2, 20, 51, 801, DateTimeKind.Unspecified).AddTicks(3747), "Lead", 24, 3, 2 },
                    { 18, new DateTime(2018, 4, 17, 18, 47, 37, 914, DateTimeKind.Unspecified).AddTicks(8056), "Corrupti vel porro provident ipsam aut rem repellat eveniet praesentium vel qui qui id.", new DateTime(2019, 9, 14, 11, 49, 25, 87, DateTimeKind.Unspecified).AddTicks(9772), "magenta", 14, 4, 2 },
                    { 39, new DateTime(2017, 1, 4, 5, 48, 38, 490, DateTimeKind.Unspecified).AddTicks(68), "Sequi nihil doloremque occaecati est est similique occaecati voluptatem explicabo ipsum.", new DateTime(2018, 8, 7, 3, 32, 45, 929, DateTimeKind.Unspecified).AddTicks(6139), "Incredible Soft Salad", 23, 4, 2 },
                    { 40, new DateTime(2018, 9, 16, 3, 48, 50, 745, DateTimeKind.Unspecified).AddTicks(433), "Ipsum magni est alias vitae vitae assumenda ut minus vitae impedit quia aut a.", new DateTime(2020, 5, 3, 3, 3, 10, 688, DateTimeKind.Unspecified).AddTicks(3900), "Tasty Metal Hat", 27, 4, 1 },
                    { 50, new DateTime(2017, 6, 10, 14, 14, 40, 9, DateTimeKind.Unspecified).AddTicks(9492), "Est repudiandae facilis doloremque officia tempore ipsa sint sunt labore magnam corporis velit est.", new DateTime(2019, 12, 24, 2, 28, 9, 256, DateTimeKind.Unspecified).AddTicks(7583), "primary", 13, 4, 3 },
                    { 49, new DateTime(2017, 5, 1, 21, 3, 36, 945, DateTimeKind.Unspecified).AddTicks(4689), "Ad ducimus quis voluptate dicta qui ab aut sint eum vitae.", new DateTime(2017, 5, 15, 22, 22, 24, 350, DateTimeKind.Unspecified).AddTicks(7515), "sexy", 9, 2, 4 },
                    { 3, new DateTime(2017, 7, 1, 15, 29, 53, 964, DateTimeKind.Unspecified).AddTicks(2644), "Dolores fugit qui facere nostrum voluptas corrupti molestiae magni sed quo.", new DateTime(2020, 4, 26, 7, 52, 11, 770, DateTimeKind.Unspecified).AddTicks(2745), "Kwacha", 17, 6, 3 },
                    { 38, new DateTime(2017, 7, 14, 17, 48, 57, 551, DateTimeKind.Unspecified).AddTicks(2392), "Natus qui excepturi fugiat et ipsa omnis autem.", new DateTime(2017, 7, 22, 21, 37, 26, 358, DateTimeKind.Unspecified).AddTicks(3933), "web-readiness", 11, 6, 1 },
                    { 15, new DateTime(2017, 7, 20, 18, 58, 9, 175, DateTimeKind.Unspecified).AddTicks(3932), "Ex et nostrum voluptates reprehenderit et voluptates enim voluptas minima aut commodi.", new DateTime(2019, 8, 22, 21, 14, 2, 461, DateTimeKind.Unspecified).AddTicks(6646), "National", 16, 8, 3 },
                    { 19, new DateTime(2017, 8, 20, 4, 3, 56, 961, DateTimeKind.Unspecified).AddTicks(5727), "Et aliquam repellat quos eum.", new DateTime(2020, 3, 16, 13, 11, 20, 317, DateTimeKind.Unspecified).AddTicks(8993), "auxiliary", 11, 8, 1 },
                    { 31, new DateTime(2018, 6, 6, 1, 43, 23, 191, DateTimeKind.Unspecified).AddTicks(9851), "Excepturi ut possimus beatae iure sed aliquam qui debitis minus et et dolorem.", new DateTime(2017, 3, 19, 18, 14, 8, 412, DateTimeKind.Unspecified).AddTicks(9678), "Gorgeous Concrete Chips", 25, 8, 2 },
                    { 46, new DateTime(2017, 10, 31, 16, 14, 42, 800, DateTimeKind.Unspecified).AddTicks(535), "Sed voluptatem ea qui enim ratione similique impedit velit cumque quis nisi vel.", new DateTime(2020, 2, 27, 21, 15, 44, 690, DateTimeKind.Unspecified).AddTicks(16), "Business-focused", 27, 8, 2 },
                    { 34, new DateTime(2017, 11, 7, 21, 15, 39, 155, DateTimeKind.Unspecified).AddTicks(1911), "Voluptates corrupti ad hic sunt non recusandae sunt laboriosam et earum.", new DateTime(2017, 5, 14, 14, 20, 40, 936, DateTimeKind.Unspecified).AddTicks(9673), "technologies", 18, 4, 1 },
                    { 1, new DateTime(2018, 9, 30, 3, 11, 48, 605, DateTimeKind.Unspecified).AddTicks(937), "Quo omnis eos aspernatur maxime adipisci a laboriosam laboriosam perspiciatis corrupti.", new DateTime(2020, 1, 6, 0, 7, 29, 589, DateTimeKind.Unspecified).AddTicks(230), "Refined", 24, 5, 1 },
                    { 2, new DateTime(2018, 3, 24, 18, 29, 38, 838, DateTimeKind.Unspecified).AddTicks(3004), "Culpa consequatur error fugiat dolores.", new DateTime(2019, 11, 29, 20, 33, 13, 257, DateTimeKind.Unspecified).AddTicks(4998), "pricing structure", 29, 5, 3 },
                    { 7, new DateTime(2018, 7, 1, 13, 53, 38, 156, DateTimeKind.Unspecified).AddTicks(3223), "Suscipit incidunt numquam impedit est enim voluptates saepe odit.", new DateTime(2019, 12, 16, 23, 45, 33, 455, DateTimeKind.Unspecified).AddTicks(7552), "deposit", 17, 5, 3 },
                    { 12, new DateTime(2018, 5, 22, 22, 35, 34, 119, DateTimeKind.Unspecified).AddTicks(6498), "Ea qui officiis ut laborum qui cumque qui laborum veritatis illum.", new DateTime(2019, 1, 2, 15, 24, 14, 156, DateTimeKind.Unspecified).AddTicks(8128), "ubiquitous", 6, 5, 2 },
                    { 20, new DateTime(2017, 10, 10, 23, 29, 28, 351, DateTimeKind.Unspecified).AddTicks(4750), "Aut dolores aut ut quaerat ullam doloribus minus odit quisquam magni dolorem.", new DateTime(2020, 1, 21, 23, 11, 45, 884, DateTimeKind.Unspecified).AddTicks(3886), "Forward", 6, 6, 1 },
                    { 6, new DateTime(2017, 6, 2, 19, 31, 58, 557, DateTimeKind.Unspecified).AddTicks(1044), "Eveniet dolores ipsa et rerum minima quo incidunt odit aut ut sunt modi quis.", new DateTime(2017, 11, 30, 0, 15, 35, 918, DateTimeKind.Unspecified).AddTicks(2980), "Latvia", 12, 2, 2 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 50);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5);
        }
    }
}
