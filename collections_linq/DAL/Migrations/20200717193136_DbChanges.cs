﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class DbChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 50);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Teams",
                newName: "TeamName");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Deadline",
                table: "Projects",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 7, new DateTime(2017, 6, 2, 3, 17, 6, 217, DateTimeKind.Unspecified).AddTicks(1309), new DateTime(2020, 10, 13, 7, 33, 52, 951, DateTimeKind.Unspecified).AddTicks(1129), "Odio in numquam dolorum et non quae provident cum.", "Glen", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 25, new DateTime(2018, 3, 27, 20, 49, 2, 82, DateTimeKind.Unspecified).AddTicks(3804), new DateTime(2020, 5, 8, 1, 6, 37, 544, DateTimeKind.Unspecified).AddTicks(4023), "Assumenda reprehenderit et voluptatem sunt sint vero voluptates repudiandae.", "Auto Loan Account", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 5, new DateTime(2017, 6, 30, 5, 51, 0, 211, DateTimeKind.Unspecified).AddTicks(648), new DateTime(2020, 11, 12, 11, 12, 7, 388, DateTimeKind.Unspecified).AddTicks(401), "Et ipsam alias quia est quisquam velit incidunt nihil ullam enim dolor.", "Home Loan Account", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 23, new DateTime(2018, 10, 2, 19, 39, 32, 426, DateTimeKind.Unspecified).AddTicks(5398), new DateTime(2020, 12, 24, 9, 37, 12, 814, DateTimeKind.Unspecified).AddTicks(4190), "Enim laboriosam voluptatem dolorem dolores necessitatibus exercitationem.", "Forks" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 9, new DateTime(2018, 10, 29, 18, 47, 19, 498, DateTimeKind.Unspecified).AddTicks(880), new DateTime(2020, 7, 27, 17, 57, 8, 6, DateTimeKind.Unspecified).AddTicks(1531), "Et et provident excepturi beatae harum.", "concept", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 7, new DateTime(2019, 8, 24, 1, 13, 19, 512, DateTimeKind.Unspecified).AddTicks(4495), new DateTime(2020, 8, 11, 4, 55, 18, 415, DateTimeKind.Unspecified).AddTicks(3893), "At corporis eos eligendi voluptatum voluptatum voluptatem expedita.", "Plastic", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 6, new DateTime(2018, 2, 1, 0, 53, 39, 867, DateTimeKind.Unspecified).AddTicks(3889), new DateTime(2021, 4, 4, 14, 1, 37, 594, DateTimeKind.Unspecified).AddTicks(4784), "Aut voluptates et incidunt aut voluptatem provident alias quo aliquid harum et.", "hack" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 30, new DateTime(2017, 3, 4, 20, 5, 1, 266, DateTimeKind.Unspecified).AddTicks(9304), new DateTime(2020, 6, 22, 3, 24, 16, 645, DateTimeKind.Unspecified).AddTicks(9772), "Eos dolorum veniam officia nesciunt saepe perferendis dolorem.", "Central", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 13, new DateTime(2017, 6, 2, 7, 38, 41, 440, DateTimeKind.Unspecified).AddTicks(3951), new DateTime(2021, 5, 6, 21, 42, 5, 654, DateTimeKind.Unspecified).AddTicks(4522), "Impedit ut occaecati optio eius debitis dolore.", "infrastructures", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 2, new DateTime(2018, 7, 27, 7, 25, 40, 791, DateTimeKind.Unspecified).AddTicks(6417), new DateTime(2021, 7, 4, 16, 50, 20, 144, DateTimeKind.Unspecified).AddTicks(7277), "Omnis fugit occaecati maxime esse aut repellendus quaerat sit saepe.", "indexing", 4 });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2019, 5, 14, 15, 27, 15, 166, DateTimeKind.Unspecified).AddTicks(5964));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2018, 5, 11, 14, 0, 28, 443, DateTimeKind.Unspecified).AddTicks(6027));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2017, 4, 11, 22, 8, 31, 207, DateTimeKind.Unspecified).AddTicks(3881));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedAt",
                value: new DateTime(2016, 3, 17, 15, 0, 49, 97, DateTimeKind.Unspecified).AddTicks(329));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedAt",
                value: new DateTime(2015, 3, 25, 8, 45, 18, 498, DateTimeKind.Unspecified).AddTicks(4700));

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 17, new DateTime(1998, 6, 27, 14, 5, 31, 147, DateTimeKind.Unspecified).AddTicks(4260), "Melissa_Howe49@hotmail.com", "Melissa", "Howe", new DateTime(2018, 9, 13, 11, 38, 32, 933, DateTimeKind.Unspecified).AddTicks(618), null },
                    { 11, new DateTime(2009, 11, 13, 5, 4, 41, 453, DateTimeKind.Unspecified).AddTicks(7356), "Nelle_Metz@hotmail.com", "Nelle", "Metz", new DateTime(2012, 3, 30, 17, 52, 6, 433, DateTimeKind.Unspecified).AddTicks(950), null },
                    { 12, new DateTime(1977, 1, 16, 3, 11, 40, 122, DateTimeKind.Unspecified).AddTicks(5003), "Makenna.Larkin41@gmail.com", "Makenna", "Larkin", new DateTime(2014, 6, 20, 7, 54, 31, 71, DateTimeKind.Unspecified).AddTicks(3751), null },
                    { 13, new DateTime(1986, 10, 2, 14, 20, 48, 861, DateTimeKind.Unspecified).AddTicks(5396), "Vincent9@gmail.com", "Vincent", "Pollich", new DateTime(2010, 11, 3, 6, 4, 30, 177, DateTimeKind.Unspecified).AddTicks(3955), null },
                    { 14, new DateTime(2000, 11, 5, 21, 58, 47, 396, DateTimeKind.Unspecified).AddTicks(9974), "Neha92@gmail.com", "Neha", "Smith", new DateTime(2019, 7, 19, 19, 57, 42, 983, DateTimeKind.Unspecified).AddTicks(1878), 2 },
                    { 15, new DateTime(1968, 10, 5, 18, 58, 49, 285, DateTimeKind.Unspecified).AddTicks(1736), "Ellis33@hotmail.com", "Ellis", "D'Amore", new DateTime(2011, 6, 7, 11, 14, 41, 590, DateTimeKind.Unspecified).AddTicks(3599), null },
                    { 16, new DateTime(1985, 4, 29, 9, 57, 37, 288, DateTimeKind.Unspecified).AddTicks(837), "Jules72@yahoo.com", "Jules", "Ward", new DateTime(2014, 3, 3, 19, 21, 31, 947, DateTimeKind.Unspecified).AddTicks(7667), 3 },
                    { 18, new DateTime(1998, 8, 16, 9, 55, 51, 361, DateTimeKind.Unspecified).AddTicks(2794), "Eldon.Donnelly@gmail.com", "Eldon", "Donnelly", new DateTime(2011, 4, 4, 8, 30, 47, 18, DateTimeKind.Unspecified).AddTicks(5291), null },
                    { 20, new DateTime(1969, 11, 10, 5, 8, 1, 826, DateTimeKind.Unspecified).AddTicks(8594), "Kiel.Breitenberg86@hotmail.com", "Kiel", "Breitenberg", new DateTime(2019, 6, 28, 1, 27, 39, 726, DateTimeKind.Unspecified).AddTicks(784), null },
                    { 9, new DateTime(1973, 10, 29, 23, 11, 33, 214, DateTimeKind.Unspecified).AddTicks(2766), "Raina_Weissnat@hotmail.com", "Raina", "Weissnat", new DateTime(2018, 6, 3, 8, 42, 44, 170, DateTimeKind.Unspecified).AddTicks(5770), null },
                    { 8, new DateTime(1982, 10, 20, 12, 25, 16, 174, DateTimeKind.Unspecified).AddTicks(3760), "Gilda_Gislason42@hotmail.com", "Gilda", "Gislason", new DateTime(2016, 11, 25, 0, 40, 25, 315, DateTimeKind.Unspecified).AddTicks(8399), 5 },
                    { 7, new DateTime(1972, 9, 29, 15, 40, 42, 169, DateTimeKind.Unspecified).AddTicks(9221), "Corbin0@yahoo.com", "Corbin", "Lakin", new DateTime(2019, 2, 11, 8, 4, 53, 804, DateTimeKind.Unspecified).AddTicks(3465), 3 },
                    { 6, new DateTime(1974, 10, 15, 6, 43, 2, 425, DateTimeKind.Unspecified).AddTicks(5132), "Jovani.Hermiston@yahoo.com", "Jovani", "Hermiston", new DateTime(2019, 2, 21, 16, 25, 57, 285, DateTimeKind.Unspecified).AddTicks(2754), 2 },
                    { 5, new DateTime(2002, 5, 21, 0, 2, 26, 133, DateTimeKind.Unspecified).AddTicks(3052), "Annabell.Mohr@gmail.com", "Annabell", "Mohr", new DateTime(2016, 5, 6, 15, 34, 37, 735, DateTimeKind.Unspecified).AddTicks(3760), null },
                    { 4, new DateTime(1984, 12, 30, 12, 41, 37, 565, DateTimeKind.Unspecified).AddTicks(7944), "Rose.Johnson@gmail.com", "Rose", "Johnson", new DateTime(2018, 9, 10, 12, 53, 51, 626, DateTimeKind.Unspecified).AddTicks(6736), null },
                    { 3, new DateTime(1978, 5, 21, 2, 8, 12, 875, DateTimeKind.Unspecified).AddTicks(1147), "Chasity74@hotmail.com", "Chasity", "Fritsch", new DateTime(2013, 5, 16, 11, 23, 25, 366, DateTimeKind.Unspecified).AddTicks(716), 3 },
                    { 2, new DateTime(1994, 7, 16, 8, 57, 58, 191, DateTimeKind.Unspecified).AddTicks(1002), "Lula.Hirthe@yahoo.com", "Lula", "Hirthe", new DateTime(2017, 8, 6, 22, 15, 30, 242, DateTimeKind.Unspecified).AddTicks(8362), 3 },
                    { 1, new DateTime(1996, 10, 9, 16, 56, 35, 185, DateTimeKind.Unspecified).AddTicks(8238), "Mable76@hotmail.com", "Mable", "Harvey", new DateTime(2014, 1, 19, 3, 54, 54, 91, DateTimeKind.Unspecified).AddTicks(9984), 2 },
                    { 30, new DateTime(1967, 11, 18, 11, 28, 14, 342, DateTimeKind.Unspecified).AddTicks(7234), "Virgie.Willms3@yahoo.com", "Virgie", "Willms", new DateTime(2013, 12, 7, 1, 33, 34, 396, DateTimeKind.Unspecified).AddTicks(5016), null },
                    { 29, new DateTime(1993, 1, 31, 4, 46, 20, 75, DateTimeKind.Unspecified).AddTicks(3198), "Elna.Hartmann8@hotmail.com", "Elna", "Hartmann", new DateTime(2013, 9, 9, 18, 53, 13, 829, DateTimeKind.Unspecified).AddTicks(3757), null },
                    { 28, new DateTime(2001, 9, 2, 12, 45, 40, 768, DateTimeKind.Unspecified).AddTicks(4988), "Baron.Deckow42@yahoo.com", "Baron", "Deckow", new DateTime(2017, 9, 29, 4, 32, 26, 687, DateTimeKind.Unspecified).AddTicks(9579), null },
                    { 27, new DateTime(1974, 1, 28, 13, 49, 17, 757, DateTimeKind.Unspecified).AddTicks(7748), "Maryam.Heathcote75@yahoo.com", "Maryam", "Heathcote", new DateTime(2019, 4, 27, 14, 33, 57, 325, DateTimeKind.Unspecified).AddTicks(3984), null },
                    { 26, new DateTime(1974, 1, 22, 9, 4, 44, 64, DateTimeKind.Unspecified).AddTicks(5778), "Celestine.Kunze@gmail.com", "Celestine", "Kunze", new DateTime(2013, 9, 26, 13, 9, 20, 133, DateTimeKind.Unspecified).AddTicks(9771), null },
                    { 25, new DateTime(1975, 8, 19, 14, 0, 30, 473, DateTimeKind.Unspecified).AddTicks(3780), "Antonio10@hotmail.com", "Antonio", "Stehr", new DateTime(2011, 7, 9, 5, 18, 56, 407, DateTimeKind.Unspecified).AddTicks(7312), 1 },
                    { 24, new DateTime(1963, 4, 18, 8, 55, 30, 688, DateTimeKind.Unspecified).AddTicks(7016), "Jamey22@gmail.com", "Jamey", "Schultz", new DateTime(2018, 4, 7, 23, 34, 53, 490, DateTimeKind.Unspecified).AddTicks(8560), 2 },
                    { 23, new DateTime(2003, 1, 12, 23, 47, 32, 911, DateTimeKind.Unspecified).AddTicks(4348), "Elva91@yahoo.com", "Elva", "Jast", new DateTime(2012, 8, 27, 3, 25, 56, 696, DateTimeKind.Unspecified).AddTicks(9042), 1 },
                    { 22, new DateTime(1968, 2, 19, 5, 40, 57, 797, DateTimeKind.Unspecified).AddTicks(1524), "Hassie_Jast@yahoo.com", "Hassie", "Jast", new DateTime(2019, 7, 21, 23, 43, 3, 257, DateTimeKind.Unspecified).AddTicks(84), 3 },
                    { 21, new DateTime(1999, 12, 10, 20, 15, 44, 850, DateTimeKind.Unspecified).AddTicks(9274), "Jaycee_Kunze@yahoo.com", "Jaycee", "Kunze", new DateTime(2013, 9, 9, 17, 9, 12, 414, DateTimeKind.Unspecified).AddTicks(3646), null },
                    { 19, new DateTime(1976, 3, 29, 2, 50, 21, 550, DateTimeKind.Unspecified).AddTicks(8792), "Loraine45@hotmail.com", "Loraine", "Hessel", new DateTime(2015, 11, 4, 6, 28, 18, 364, DateTimeKind.Unspecified).AddTicks(2990), 3 },
                    { 10, new DateTime(1986, 8, 11, 5, 53, 2, 756, DateTimeKind.Unspecified).AddTicks(3667), "Jada.Hegmann@hotmail.com", "Jada", "Hegmann", new DateTime(2016, 7, 3, 13, 50, 7, 98, DateTimeKind.Unspecified).AddTicks(9572), 3 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[,]
                {
                    { 9, new DateTime(2018, 1, 31, 0, 14, 19, 773, DateTimeKind.Unspecified).AddTicks(8523), "Rem et vel cumque et magni.", new DateTime(2019, 3, 17, 1, 47, 30, 553, DateTimeKind.Unspecified).AddTicks(1595), "Analyst", 10, 10, 2 },
                    { 33, new DateTime(2018, 4, 8, 4, 28, 7, 162, DateTimeKind.Unspecified).AddTicks(2369), "Hic consequuntur unde error qui culpa ea dolore ut eveniet debitis ipsa.", new DateTime(2017, 11, 14, 8, 39, 17, 867, DateTimeKind.Unspecified).AddTicks(43), "Ergonomic", 23, 5, 2 },
                    { 20, new DateTime(2018, 2, 6, 2, 50, 55, 428, DateTimeKind.Unspecified).AddTicks(7079), "Molestiae est adipisci officiis corporis a laudantium optio possimus reiciendis ut veritatis explicabo.", new DateTime(2020, 7, 12, 10, 34, 0, 383, DateTimeKind.Unspecified).AddTicks(2708), "Ergonomic", 23, 3, 2 },
                    { 15, new DateTime(2018, 6, 14, 23, 46, 40, 47, DateTimeKind.Unspecified).AddTicks(7906), "Quia inventore rerum eum quis nostrum esse distinctio sint qui nobis sed.", new DateTime(2020, 5, 26, 0, 12, 39, 189, DateTimeKind.Unspecified).AddTicks(9730), "Borders", 23, 5, 4 },
                    { 3, new DateTime(2018, 7, 8, 1, 6, 51, 377, DateTimeKind.Unspecified).AddTicks(5197), "Laboriosam et esse provident dolorem.", new DateTime(2017, 3, 7, 14, 15, 39, 224, DateTimeKind.Unspecified).AddTicks(8505), "incentivize", 23, 2, 4 },
                    { 46, new DateTime(2017, 4, 20, 21, 53, 38, 936, DateTimeKind.Unspecified).AddTicks(7110), "Delectus rerum perspiciatis enim suscipit voluptatum.", new DateTime(2017, 4, 16, 17, 53, 54, 218, DateTimeKind.Unspecified).AddTicks(1536), "index", 23, 9, 1 },
                    { 39, new DateTime(2018, 9, 26, 12, 45, 13, 679, DateTimeKind.Unspecified).AddTicks(3276), "Quod provident cum beatae consequuntur illo.", new DateTime(2020, 3, 30, 20, 19, 37, 394, DateTimeKind.Unspecified).AddTicks(7991), "primary", 22, 1, 4 },
                    { 50, new DateTime(2017, 2, 26, 6, 40, 18, 326, DateTimeKind.Unspecified).AddTicks(3026), "Sunt facere itaque voluptatibus aut eligendi fugit incidunt deleniti totam debitis sit aut.", new DateTime(2019, 10, 23, 17, 6, 34, 571, DateTimeKind.Unspecified).AddTicks(426), "Consultant", 22, 9, 3 },
                    { 34, new DateTime(2017, 10, 14, 5, 15, 7, 478, DateTimeKind.Unspecified).AddTicks(5440), "Nihil laudantium exercitationem molestiae maiores consequatur et porro corrupti.", new DateTime(2019, 4, 12, 13, 44, 48, 253, DateTimeKind.Unspecified).AddTicks(6342), "bluetooth", 21, 9, 4 },
                    { 44, new DateTime(2017, 3, 17, 0, 47, 35, 920, DateTimeKind.Unspecified).AddTicks(7408), "Unde quibusdam veritatis corporis voluptates recusandae quo qui consequatur fugit sit distinctio.", new DateTime(2018, 11, 25, 1, 37, 2, 597, DateTimeKind.Unspecified).AddTicks(2938), "bottom-line", 21, 2, 3 },
                    { 14, new DateTime(2017, 12, 4, 19, 57, 55, 173, DateTimeKind.Unspecified).AddTicks(6871), "Ea accusamus autem qui quam labore perferendis cupiditate fugit itaque et.", new DateTime(2020, 7, 16, 1, 30, 7, 732, DateTimeKind.Unspecified).AddTicks(1012), "Mall", 19, 9, 2 },
                    { 10, new DateTime(2017, 7, 20, 19, 7, 12, 91, DateTimeKind.Unspecified).AddTicks(7808), "Ad officiis odio id non et nisi veniam magnam.", new DateTime(2019, 10, 31, 13, 29, 26, 238, DateTimeKind.Unspecified).AddTicks(8324), "Money Market Account", 18, 5, 2 },
                    { 41, new DateTime(2017, 9, 16, 8, 12, 27, 535, DateTimeKind.Unspecified).AddTicks(3408), "Nesciunt consequatur autem et et.", new DateTime(2018, 1, 12, 13, 41, 34, 408, DateTimeKind.Unspecified).AddTicks(1182), "calculating", 18, 2, 1 },
                    { 23, new DateTime(2018, 7, 3, 10, 36, 7, 13, DateTimeKind.Unspecified).AddTicks(4364), "Recusandae rerum aut illum explicabo aut expedita architecto aut blanditiis animi.", new DateTime(2018, 4, 1, 2, 21, 5, 70, DateTimeKind.Unspecified).AddTicks(1717), "Handmade Wooden Salad", 15, 7, 4 },
                    { 38, new DateTime(2018, 6, 21, 20, 41, 55, 24, DateTimeKind.Unspecified).AddTicks(4672), "Nam consequuntur sunt quo recusandae dolor et.", new DateTime(2018, 12, 17, 22, 28, 0, 157, DateTimeKind.Unspecified).AddTicks(8326), "orchestration", 15, 4, 3 },
                    { 47, new DateTime(2018, 3, 31, 6, 16, 40, 783, DateTimeKind.Unspecified).AddTicks(1560), "Quos debitis vero nobis consequuntur enim totam veniam nobis alias mollitia saepe fugit.", new DateTime(2020, 5, 21, 12, 42, 7, 751, DateTimeKind.Unspecified).AddTicks(2900), "Future", 15, 3, 2 },
                    { 13, new DateTime(2017, 8, 2, 7, 58, 38, 691, DateTimeKind.Unspecified).AddTicks(5159), "Quis fugit modi rerum mollitia quae.", new DateTime(2020, 4, 26, 21, 48, 42, 393, DateTimeKind.Unspecified).AddTicks(8800), "Village", 14, 9, 2 },
                    { 4, new DateTime(2018, 5, 5, 10, 59, 49, 132, DateTimeKind.Unspecified).AddTicks(1769), "Laboriosam facere in molestiae et qui est quibusdam beatae.", new DateTime(2018, 8, 14, 12, 13, 27, 77, DateTimeKind.Unspecified).AddTicks(5631), "Idaho", 14, 4, 2 },
                    { 42, new DateTime(2017, 3, 8, 17, 3, 26, 870, DateTimeKind.Unspecified).AddTicks(6560), "Iusto voluptatem deserunt vel itaque omnis omnis velit consequatur quae tempore dolores cupiditate.", new DateTime(2017, 3, 28, 9, 45, 17, 154, DateTimeKind.Unspecified).AddTicks(1966), "input", 13, 3, 3 },
                    { 24, new DateTime(2018, 4, 13, 14, 27, 43, 863, DateTimeKind.Unspecified).AddTicks(3121), "Aut nulla blanditiis possimus impedit fuga occaecati quia officia.", new DateTime(2020, 1, 20, 17, 43, 16, 519, DateTimeKind.Unspecified).AddTicks(6272), "Freeway", 12, 4, 1 },
                    { 2, new DateTime(2018, 11, 13, 18, 5, 52, 731, DateTimeKind.Unspecified).AddTicks(4504), "Quo sed quos alias voluptatem rerum ratione cupiditate consequatur repellat rerum quae distinctio.", new DateTime(2017, 8, 15, 12, 49, 35, 867, DateTimeKind.Unspecified).AddTicks(8221), "Creek", 12, 9, 2 },
                    { 35, new DateTime(2018, 11, 7, 15, 34, 21, 128, DateTimeKind.Unspecified).AddTicks(538), "Itaque quas ut repellat ut.", new DateTime(2019, 9, 15, 22, 23, 10, 140, DateTimeKind.Unspecified).AddTicks(4215), "Beauty & Automotive", 12, 8, 3 },
                    { 18, new DateTime(2017, 8, 26, 8, 4, 55, 827, DateTimeKind.Unspecified).AddTicks(3502), "Sed neque et adipisci magni rerum veritatis delectus quidem quos sint aspernatur ut.", new DateTime(2018, 9, 18, 7, 58, 2, 901, DateTimeKind.Unspecified).AddTicks(341), "Practical Metal Table", 24, 3, 4 },
                    { 32, new DateTime(2017, 9, 10, 9, 44, 39, 110, DateTimeKind.Unspecified).AddTicks(3288), "Sunt sed in eius quos nam adipisci minima optio magnam.", new DateTime(2017, 11, 2, 15, 26, 31, 480, DateTimeKind.Unspecified).AddTicks(2542), "empowering", 27, 1, 3 },
                    { 28, new DateTime(2018, 11, 14, 14, 54, 14, 768, DateTimeKind.Unspecified).AddTicks(5843), "Possimus repellendus omnis dolor atque recusandae voluptates temporibus accusamus unde alias sequi.", new DateTime(2018, 10, 23, 16, 41, 53, 409, DateTimeKind.Unspecified).AddTicks(6315), "connect", 28, 10, 3 },
                    { 45, new DateTime(2018, 11, 10, 1, 58, 53, 878, DateTimeKind.Unspecified).AddTicks(9914), "Atque in tenetur et rerum nam.", new DateTime(2018, 5, 15, 2, 16, 6, 316, DateTimeKind.Unspecified).AddTicks(1642), "success", 30, 7, 2 },
                    { 8, new DateTime(2017, 10, 2, 10, 44, 4, 64, DateTimeKind.Unspecified).AddTicks(8825), "Dignissimos sapiente totam in repudiandae molestias nihil quos possimus.", new DateTime(2019, 11, 4, 21, 20, 44, 837, DateTimeKind.Unspecified).AddTicks(6702), "Home Loan Account", 10, 9, 4 },
                    { 22, new DateTime(2018, 9, 24, 9, 8, 33, 328, DateTimeKind.Unspecified).AddTicks(7538), "Et maxime quasi id quaerat mollitia sed.", new DateTime(2018, 9, 25, 11, 47, 2, 850, DateTimeKind.Unspecified).AddTicks(4474), "Metal", 9, 7, 1 },
                    { 16, new DateTime(2018, 2, 28, 12, 49, 19, 443, DateTimeKind.Unspecified).AddTicks(8365), "Numquam at dolor quia quia nesciunt rem.", new DateTime(2018, 12, 16, 13, 49, 23, 983, DateTimeKind.Unspecified).AddTicks(2583), "Refined Steel Salad", 9, 7, 3 },
                    { 1, new DateTime(2018, 5, 29, 21, 10, 26, 689, DateTimeKind.Unspecified).AddTicks(7363), "Soluta tempora numquam praesentium odit numquam dolore quia minus.", new DateTime(2020, 5, 11, 12, 28, 59, 584, DateTimeKind.Unspecified).AddTicks(3650), "Intelligent Plastic Chips", 9, 6, 3 },
                    { 25, new DateTime(2018, 7, 23, 2, 37, 40, 942, DateTimeKind.Unspecified).AddTicks(2102), "Dolor dicta ipsam consequatur consequatur commodi dicta repellat vero.", new DateTime(2020, 7, 14, 2, 25, 54, 392, DateTimeKind.Unspecified).AddTicks(2139), "alarm", 8, 7, 4 },
                    { 21, new DateTime(2017, 7, 25, 19, 21, 8, 254, DateTimeKind.Unspecified).AddTicks(130), "Non dolores delectus eius illo non beatae aut fuga officiis cupiditate placeat.", new DateTime(2019, 8, 20, 13, 54, 49, 583, DateTimeKind.Unspecified).AddTicks(7571), "invoice", 8, 3, 3 },
                    { 12, new DateTime(2018, 7, 3, 19, 19, 48, 949, DateTimeKind.Unspecified).AddTicks(2329), "Sint commodi aliquam natus optio dolorum ut modi qui sit qui reprehenderit.", new DateTime(2019, 5, 10, 20, 51, 0, 210, DateTimeKind.Unspecified).AddTicks(5876), "invoice", 8, 2, 2 },
                    { 6, new DateTime(2018, 2, 15, 0, 23, 9, 269, DateTimeKind.Unspecified).AddTicks(4718), "Nostrum perspiciatis animi error impedit et iure maxime.", new DateTime(2019, 5, 29, 15, 17, 11, 184, DateTimeKind.Unspecified).AddTicks(9170), "Creative", 8, 9, 3 },
                    { 49, new DateTime(2017, 5, 30, 14, 21, 32, 621, DateTimeKind.Unspecified).AddTicks(9540), "Quisquam veniam earum hic autem modi fugiat velit provident nulla molestiae.", new DateTime(2017, 2, 7, 15, 13, 38, 386, DateTimeKind.Unspecified).AddTicks(7173), "Ergonomic Granite Computer", 8, 5, 2 },
                    { 31, new DateTime(2017, 6, 16, 22, 7, 0, 835, DateTimeKind.Unspecified).AddTicks(8420), "Odit soluta temporibus sunt dolor facilis ipsam delectus voluptas rerum qui magni.", new DateTime(2020, 6, 4, 13, 8, 49, 256, DateTimeKind.Unspecified).AddTicks(7952), "payment", 6, 10, 4 },
                    { 11, new DateTime(2018, 8, 25, 20, 14, 47, 201, DateTimeKind.Unspecified).AddTicks(6725), "Tempore et impedit ratione vitae nesciunt est quos qui aut ipsam voluptate.", new DateTime(2019, 9, 30, 22, 1, 29, 619, DateTimeKind.Unspecified).AddTicks(7428), "Mississippi", 11, 9, 1 },
                    { 19, new DateTime(2017, 4, 11, 3, 41, 12, 643, DateTimeKind.Unspecified).AddTicks(3637), "Dolores labore rerum animi maiores illo.", new DateTime(2018, 7, 22, 0, 47, 8, 130, DateTimeKind.Unspecified).AddTicks(2595), "India", 6, 3, 4 },
                    { 5, new DateTime(2017, 2, 13, 3, 39, 31, 28, DateTimeKind.Unspecified).AddTicks(1031), "Unde similique ipsum autem enim alias quia.", new DateTime(2017, 6, 6, 15, 6, 57, 836, DateTimeKind.Unspecified).AddTicks(1761), "Concrete", 6, 7, 4 },
                    { 37, new DateTime(2018, 6, 15, 22, 3, 21, 304, DateTimeKind.Unspecified).AddTicks(2224), "Sint voluptas eveniet accusamus ea excepturi nihil et inventore aut.", new DateTime(2018, 3, 1, 9, 53, 16, 373, DateTimeKind.Unspecified).AddTicks(3026), "US Dollar", 6, 10, 4 },
                    { 27, new DateTime(2017, 4, 7, 22, 7, 24, 26, DateTimeKind.Unspecified).AddTicks(6235), "Fugit iusto ad sit quasi occaecati.", new DateTime(2019, 4, 19, 19, 34, 37, 706, DateTimeKind.Unspecified).AddTicks(9756), "Manager", 5, 9, 2 },
                    { 40, new DateTime(2018, 4, 11, 0, 31, 9, 512, DateTimeKind.Unspecified).AddTicks(4918), "Et rerum laborum voluptatem ut enim eaque eveniet quasi in voluptatem maiores veritatis.", new DateTime(2017, 5, 29, 21, 19, 32, 766, DateTimeKind.Unspecified).AddTicks(5514), "array", 5, 5, 3 },
                    { 29, new DateTime(2018, 8, 10, 6, 55, 6, 425, DateTimeKind.Unspecified).AddTicks(2880), "Doloribus nam est aut voluptas eligendi nemo quis mollitia.", new DateTime(2017, 2, 5, 13, 27, 57, 867, DateTimeKind.Unspecified).AddTicks(9360), "Centralized", 3, 8, 3 },
                    { 26, new DateTime(2018, 7, 1, 16, 46, 27, 299, DateTimeKind.Unspecified).AddTicks(4406), "Aut minus voluptatum repellendus consequatur quia recusandae aliquid ullam.", new DateTime(2018, 1, 13, 13, 29, 11, 317, DateTimeKind.Unspecified).AddTicks(9307), "generate", 2, 9, 2 },
                    { 36, new DateTime(2018, 8, 20, 5, 8, 51, 498, DateTimeKind.Unspecified).AddTicks(2858), "Dolorum in enim sint qui.", new DateTime(2019, 4, 30, 2, 44, 11, 392, DateTimeKind.Unspecified).AddTicks(3334), "core", 2, 1, 3 },
                    { 43, new DateTime(2018, 11, 30, 7, 58, 38, 558, DateTimeKind.Unspecified).AddTicks(9477), "Repellat atque ea facilis voluptatem maiores reprehenderit.", new DateTime(2017, 1, 28, 20, 48, 55, 633, DateTimeKind.Unspecified).AddTicks(9819), "users", 2, 1, 3 },
                    { 30, new DateTime(2018, 10, 2, 19, 49, 14, 875, DateTimeKind.Unspecified).AddTicks(8049), "Maiores quod eius qui non ut perspiciatis dolore rerum enim doloremque aut consequatur.", new DateTime(2019, 1, 17, 6, 23, 36, 946, DateTimeKind.Unspecified).AddTicks(9073), "Product", 1, 8, 3 },
                    { 17, new DateTime(2017, 3, 17, 1, 30, 41, 500, DateTimeKind.Unspecified).AddTicks(7631), "Vel ex non et dicta voluptate omnis quisquam et quo in consectetur.", new DateTime(2018, 3, 4, 0, 51, 29, 845, DateTimeKind.Unspecified).AddTicks(674), "override", 1, 7, 2 },
                    { 7, new DateTime(2017, 3, 20, 10, 52, 41, 345, DateTimeKind.Unspecified).AddTicks(6014), "Delectus fugit minima doloribus nam nostrum voluptatem.", new DateTime(2018, 3, 11, 10, 45, 32, 611, DateTimeKind.Unspecified).AddTicks(6707), "Incredible Soft Tuna", 6, 4, 2 },
                    { 48, new DateTime(2018, 4, 14, 8, 15, 15, 371, DateTimeKind.Unspecified).AddTicks(832), "Voluptas eligendi neque doloribus accusantium iste occaecati non corporis culpa enim deleniti.", new DateTime(2019, 3, 31, 6, 33, 51, 334, DateTimeKind.Unspecified).AddTicks(2152), "Arkansas", 11, 3, 1 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 50);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.RenameColumn(
                name: "TeamName",
                table: "Teams",
                newName: "Name");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Deadline",
                table: "Projects",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 2, new DateTime(2017, 6, 23, 7, 41, 42, 380, DateTimeKind.Unspecified).AddTicks(3914), new DateTime(2021, 9, 13, 7, 46, 12, 193, DateTimeKind.Unspecified).AddTicks(2512), "Vero rem quis quis dolores neque.", "expedite", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 27, new DateTime(2017, 11, 15, 16, 50, 50, 328, DateTimeKind.Unspecified).AddTicks(6316), new DateTime(2020, 4, 10, 7, 17, 34, 221, DateTimeKind.Unspecified).AddTicks(1968), "Aut cum repudiandae et nam facilis natus porro est eveniet soluta distinctio eaque quis.", "Intelligent Metal Mouse", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 18, new DateTime(2018, 12, 26, 9, 36, 59, 567, DateTimeKind.Unspecified).AddTicks(2971), new DateTime(2021, 2, 1, 3, 27, 51, 150, DateTimeKind.Unspecified).AddTicks(9494), "Dolorem nam laboriosam excepturi sed asperiores harum alias.", "Plastic", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 30, new DateTime(2018, 6, 6, 15, 8, 15, 941, DateTimeKind.Unspecified).AddTicks(5317), new DateTime(2021, 3, 19, 18, 0, 5, 648, DateTimeKind.Unspecified).AddTicks(1186), "Voluptas ut voluptas totam aut ut et eos placeat.", "attitude-oriented" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 17, new DateTime(2018, 8, 31, 3, 46, 14, 437, DateTimeKind.Unspecified).AddTicks(9852), new DateTime(2020, 2, 11, 7, 12, 59, 630, DateTimeKind.Unspecified).AddTicks(9741), "Molestiae quasi ea consequatur quas quae eius laboriosam in cumque tempore aut.", "Avon", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 5, new DateTime(2019, 9, 28, 21, 6, 3, 407, DateTimeKind.Unspecified).AddTicks(1570), new DateTime(2021, 9, 21, 23, 16, 38, 84, DateTimeKind.Unspecified).AddTicks(9626), "Ab provident reprehenderit quis tempore eum odit architecto est.", "orchid", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 26, new DateTime(2017, 11, 26, 19, 31, 56, 318, DateTimeKind.Unspecified).AddTicks(3719), new DateTime(2020, 7, 7, 1, 46, 12, 593, DateTimeKind.Unspecified).AddTicks(8022), "Dignissimos id rem eveniet exercitationem aut itaque dolorum error commodi voluptas.", "International" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 26, new DateTime(2018, 6, 25, 5, 51, 9, 25, DateTimeKind.Unspecified).AddTicks(1389), new DateTime(2021, 12, 13, 14, 42, 25, 241, DateTimeKind.Unspecified).AddTicks(9155), "Doloremque sit architecto magnam dolorem omnis qui reiciendis cumque molestiae ea corporis voluptatum.", "Security", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 10, new DateTime(2019, 6, 26, 14, 59, 8, 557, DateTimeKind.Unspecified).AddTicks(2545), new DateTime(2020, 1, 6, 8, 35, 50, 303, DateTimeKind.Unspecified).AddTicks(4319), "Impedit quod veniam dolores fugit voluptates.", "back-end", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 4, new DateTime(2018, 9, 26, 11, 37, 6, 109, DateTimeKind.Unspecified).AddTicks(608), new DateTime(2020, 8, 21, 8, 2, 31, 718, DateTimeKind.Unspecified).AddTicks(131), "Id ut voluptatem debitis qui quibusdam mollitia ad sit beatae sint deserunt culpa.", "Senior", 5 });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2016, 10, 6, 16, 51, 12, 275, DateTimeKind.Unspecified).AddTicks(5323));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2015, 12, 17, 7, 4, 31, 490, DateTimeKind.Unspecified).AddTicks(2486));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2017, 11, 4, 18, 23, 58, 123, DateTimeKind.Unspecified).AddTicks(7930));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedAt",
                value: new DateTime(2018, 3, 19, 5, 33, 58, 0, DateTimeKind.Unspecified).AddTicks(3675));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedAt",
                value: new DateTime(2015, 12, 18, 9, 28, 14, 545, DateTimeKind.Unspecified).AddTicks(3792));

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 17, new DateTime(1986, 2, 22, 13, 30, 44, 346, DateTimeKind.Unspecified).AddTicks(2315), "Austyn.Hilpert18@yahoo.com", "Austyn", "Hilpert", new DateTime(2010, 7, 28, 22, 27, 42, 32, DateTimeKind.Unspecified).AddTicks(1644), null },
                    { 11, new DateTime(1989, 4, 2, 17, 30, 50, 875, DateTimeKind.Unspecified).AddTicks(5396), "Devonte_Kuhlman@yahoo.com", "Devonte", "Kuhlman", new DateTime(2019, 2, 2, 6, 47, 44, 261, DateTimeKind.Unspecified).AddTicks(7580), 1 },
                    { 12, new DateTime(1962, 4, 28, 13, 52, 1, 616, DateTimeKind.Unspecified).AddTicks(1096), "Emmitt.Price@gmail.com", "Emmitt", "Price", new DateTime(2017, 7, 9, 18, 22, 27, 492, DateTimeKind.Unspecified).AddTicks(9201), 5 },
                    { 13, new DateTime(1972, 12, 9, 6, 19, 47, 928, DateTimeKind.Unspecified).AddTicks(7111), "Norma39@gmail.com", "Norma", "Steuber", new DateTime(2019, 7, 13, 20, 18, 9, 971, DateTimeKind.Unspecified).AddTicks(2299), null },
                    { 14, new DateTime(2002, 5, 2, 12, 1, 18, 288, DateTimeKind.Unspecified).AddTicks(686), "Marian39@hotmail.com", "Marian", "Muller", new DateTime(2012, 2, 27, 9, 29, 26, 451, DateTimeKind.Unspecified).AddTicks(3983), null },
                    { 15, new DateTime(1993, 3, 16, 15, 4, 7, 873, DateTimeKind.Unspecified).AddTicks(5952), "Saul.Jast72@gmail.com", "Saul", "Jast", new DateTime(2017, 8, 27, 11, 48, 33, 160, DateTimeKind.Unspecified).AddTicks(7698), null },
                    { 16, new DateTime(1965, 5, 14, 18, 30, 9, 86, DateTimeKind.Unspecified).AddTicks(5773), "Wilber_Douglas0@gmail.com", "Wilber", "Douglas", new DateTime(2017, 9, 20, 8, 4, 30, 463, DateTimeKind.Unspecified).AddTicks(3902), null },
                    { 18, new DateTime(1984, 12, 3, 7, 2, 11, 412, DateTimeKind.Unspecified).AddTicks(798), "Ida_Flatley42@hotmail.com", "Ida", "Flatley", new DateTime(2020, 7, 6, 5, 47, 54, 532, DateTimeKind.Unspecified).AddTicks(6376), 3 },
                    { 20, new DateTime(1969, 4, 29, 16, 57, 40, 302, DateTimeKind.Unspecified).AddTicks(8314), "Colton54@gmail.com", "Colton", "Greenfelder", new DateTime(2015, 4, 12, 6, 12, 20, 104, DateTimeKind.Unspecified).AddTicks(616), null },
                    { 9, new DateTime(1986, 4, 26, 10, 19, 52, 599, DateTimeKind.Unspecified).AddTicks(2885), "Nova87@yahoo.com", "Nova", "Carter", new DateTime(2010, 6, 30, 19, 16, 33, 575, DateTimeKind.Unspecified).AddTicks(1551), 5 },
                    { 8, new DateTime(1991, 4, 12, 22, 40, 13, 106, DateTimeKind.Unspecified).AddTicks(2678), "Clair43@hotmail.com", "Clair", "McDermott", new DateTime(2019, 9, 19, 12, 54, 2, 386, DateTimeKind.Unspecified).AddTicks(2498), 4 },
                    { 7, new DateTime(1982, 5, 29, 15, 6, 26, 598, DateTimeKind.Unspecified).AddTicks(2905), "Bailey.Dooley@hotmail.com", "Bailey", "Dooley", new DateTime(2013, 8, 30, 23, 45, 27, 881, DateTimeKind.Unspecified).AddTicks(5346), 5 },
                    { 6, new DateTime(1970, 9, 24, 22, 10, 36, 993, DateTimeKind.Unspecified).AddTicks(3098), "Vladimir81@gmail.com", "Vladimir", "Reichel", new DateTime(2016, 5, 13, 15, 47, 38, 293, DateTimeKind.Unspecified).AddTicks(543), null },
                    { 5, new DateTime(1990, 8, 2, 12, 4, 59, 346, DateTimeKind.Unspecified).AddTicks(6162), "Tierra45@hotmail.com", "Tierra", "Stracke", new DateTime(2013, 7, 21, 10, 48, 29, 154, DateTimeKind.Unspecified).AddTicks(9377), null },
                    { 4, new DateTime(2009, 5, 19, 18, 14, 46, 831, DateTimeKind.Unspecified).AddTicks(682), "Jess.Upton57@hotmail.com", "Jess", "Upton", new DateTime(2017, 3, 30, 6, 24, 48, 297, DateTimeKind.Unspecified).AddTicks(137), 4 },
                    { 3, new DateTime(2008, 8, 19, 2, 55, 28, 821, DateTimeKind.Unspecified).AddTicks(7132), "Fred_Marks21@yahoo.com", "Fred", "Marks", new DateTime(2013, 4, 6, 14, 47, 27, 496, DateTimeKind.Unspecified).AddTicks(1604), null },
                    { 2, new DateTime(1966, 1, 9, 10, 15, 50, 984, DateTimeKind.Unspecified).AddTicks(1033), "Oscar_Nikolaus25@hotmail.com", "Oscar", "Nikolaus", new DateTime(2012, 8, 3, 1, 44, 17, 903, DateTimeKind.Unspecified).AddTicks(9286), 5 },
                    { 1, new DateTime(1997, 5, 29, 5, 54, 25, 934, DateTimeKind.Unspecified).AddTicks(5518), "Columbus_Walter@hotmail.com", "Columbus", "Walter", new DateTime(2011, 11, 16, 10, 2, 54, 240, DateTimeKind.Unspecified).AddTicks(4290), 4 },
                    { 30, new DateTime(1961, 5, 2, 18, 33, 28, 369, DateTimeKind.Unspecified).AddTicks(4635), "Janis.Legros32@gmail.com", "Janis", "Legros", new DateTime(2013, 3, 12, 14, 46, 30, 255, DateTimeKind.Unspecified).AddTicks(3839), 4 },
                    { 29, new DateTime(2009, 1, 2, 6, 35, 54, 522, DateTimeKind.Unspecified).AddTicks(2616), "Gaston.Wolff@hotmail.com", "Gaston", "Wolff", new DateTime(2018, 9, 3, 16, 32, 53, 37, DateTimeKind.Unspecified).AddTicks(2080), 2 },
                    { 28, new DateTime(2009, 5, 11, 16, 47, 27, 580, DateTimeKind.Unspecified).AddTicks(7470), "Marley57@gmail.com", "Marley", "Orn", new DateTime(2011, 2, 14, 23, 38, 51, 865, DateTimeKind.Unspecified).AddTicks(7741), null },
                    { 27, new DateTime(2008, 2, 8, 16, 42, 46, 929, DateTimeKind.Unspecified).AddTicks(8452), "Darlene_Olson@yahoo.com", "Darlene", "Olson", new DateTime(2015, 3, 15, 6, 0, 11, 332, DateTimeKind.Unspecified).AddTicks(1225), null },
                    { 26, new DateTime(1997, 10, 26, 18, 39, 23, 475, DateTimeKind.Unspecified).AddTicks(3228), "Francesca_Pouros92@gmail.com", "Francesca", "Pouros", new DateTime(2011, 3, 12, 16, 43, 32, 341, DateTimeKind.Unspecified).AddTicks(882), null },
                    { 25, new DateTime(1970, 7, 21, 2, 45, 20, 299, DateTimeKind.Unspecified).AddTicks(5660), "Glenda82@gmail.com", "Glenda", "Terry", new DateTime(2016, 5, 26, 20, 23, 16, 348, DateTimeKind.Unspecified).AddTicks(6290), 2 },
                    { 24, new DateTime(1983, 8, 15, 14, 21, 13, 505, DateTimeKind.Unspecified).AddTicks(8637), "Eriberto.Collins@gmail.com", "Eriberto", "Collins", new DateTime(2012, 8, 15, 13, 31, 41, 666, DateTimeKind.Unspecified).AddTicks(3237), null },
                    { 23, new DateTime(1997, 5, 16, 3, 14, 32, 305, DateTimeKind.Unspecified).AddTicks(306), "Gia53@gmail.com", "Gia", "Schoen", new DateTime(2011, 12, 10, 23, 24, 40, 268, DateTimeKind.Unspecified).AddTicks(633), null },
                    { 22, new DateTime(1973, 1, 15, 2, 42, 45, 219, DateTimeKind.Unspecified).AddTicks(2564), "Dexter.Adams@yahoo.com", "Dexter", "Adams", new DateTime(2015, 4, 10, 8, 55, 2, 820, DateTimeKind.Unspecified).AddTicks(5887), null },
                    { 21, new DateTime(2008, 8, 31, 9, 35, 51, 705, DateTimeKind.Unspecified).AddTicks(5046), "Jairo_Donnelly@gmail.com", "Jairo", "Donnelly", new DateTime(2018, 8, 18, 3, 54, 40, 470, DateTimeKind.Unspecified).AddTicks(4211), null },
                    { 19, new DateTime(1963, 11, 9, 23, 20, 37, 455, DateTimeKind.Unspecified).AddTicks(9838), "Jean_OReilly@hotmail.com", "Jean", "O'Reilly", new DateTime(2016, 11, 5, 17, 35, 27, 396, DateTimeKind.Unspecified).AddTicks(3466), 4 },
                    { 10, new DateTime(1996, 7, 6, 2, 34, 34, 745, DateTimeKind.Unspecified).AddTicks(4732), "Herman_Mertz@gmail.com", "Herman", "Mertz", new DateTime(2019, 3, 10, 15, 53, 35, 330, DateTimeKind.Unspecified).AddTicks(5284), null }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "StateId" },
                values: new object[,]
                {
                    { 33, new DateTime(2018, 12, 8, 20, 27, 53, 331, DateTimeKind.Unspecified).AddTicks(5870), "Aut ratione ea quidem laborum in est dolores omnis ullam voluptatibus.", new DateTime(2017, 8, 8, 1, 40, 23, 449, DateTimeKind.Unspecified).AddTicks(7096), "Buckinghamshire", 10, 5, 4 },
                    { 43, new DateTime(2018, 11, 14, 0, 17, 25, 667, DateTimeKind.Unspecified).AddTicks(8753), "Ab quia quos veniam sit dolor accusantium.", new DateTime(2018, 9, 19, 2, 20, 51, 801, DateTimeKind.Unspecified).AddTicks(3747), "Lead", 24, 3, 2 },
                    { 39, new DateTime(2017, 1, 4, 5, 48, 38, 490, DateTimeKind.Unspecified).AddTicks(68), "Sequi nihil doloremque occaecati est est similique occaecati voluptatem explicabo ipsum.", new DateTime(2018, 8, 7, 3, 32, 45, 929, DateTimeKind.Unspecified).AddTicks(6139), "Incredible Soft Salad", 23, 4, 2 },
                    { 9, new DateTime(2017, 8, 28, 18, 59, 33, 576, DateTimeKind.Unspecified).AddTicks(7459), "Velit vel omnis et natus ut dolorem explicabo exercitationem consequatur et.", new DateTime(2020, 7, 16, 8, 28, 13, 244, DateTimeKind.Unspecified).AddTicks(8672), "SMS", 21, 3, 4 },
                    { 48, new DateTime(2018, 9, 4, 17, 8, 33, 523, DateTimeKind.Unspecified).AddTicks(3883), "Quaerat et reiciendis et molestiae doloremque fugiat ut illum distinctio.", new DateTime(2017, 2, 25, 5, 0, 7, 417, DateTimeKind.Unspecified).AddTicks(2097), "Pike", 19, 9, 4 },
                    { 47, new DateTime(2017, 2, 13, 1, 18, 39, 774, DateTimeKind.Unspecified).AddTicks(4019), "Sit placeat aut exercitationem et molestiae.", new DateTime(2018, 5, 30, 8, 1, 34, 985, DateTimeKind.Unspecified).AddTicks(906), "Platinum", 19, 6, 2 },
                    { 34, new DateTime(2017, 11, 7, 21, 15, 39, 155, DateTimeKind.Unspecified).AddTicks(1911), "Voluptates corrupti ad hic sunt non recusandae sunt laboriosam et earum.", new DateTime(2017, 5, 14, 14, 20, 40, 936, DateTimeKind.Unspecified).AddTicks(9673), "technologies", 18, 4, 1 },
                    { 28, new DateTime(2018, 11, 8, 14, 48, 42, 69, DateTimeKind.Unspecified).AddTicks(6042), "Et sunt quisquam ratione rerum iste ut maxime.", new DateTime(2017, 4, 29, 9, 16, 20, 792, DateTimeKind.Unspecified).AddTicks(1407), "FTP", 18, 7, 3 },
                    { 32, new DateTime(2018, 9, 10, 9, 43, 52, 747, DateTimeKind.Unspecified).AddTicks(719), "Velit id vero deleniti magni illum quia quia debitis ut.", new DateTime(2018, 9, 26, 20, 29, 59, 189, DateTimeKind.Unspecified).AddTicks(6190), "Licensed", 17, 3, 2 },
                    { 30, new DateTime(2017, 3, 28, 17, 33, 18, 269, DateTimeKind.Unspecified).AddTicks(1076), "Veritatis blanditiis praesentium error libero.", new DateTime(2019, 7, 15, 11, 49, 5, 440, DateTimeKind.Unspecified).AddTicks(8456), "deposit", 17, 10, 4 },
                    { 11, new DateTime(2017, 9, 30, 8, 34, 5, 496, DateTimeKind.Unspecified).AddTicks(5974), "Incidunt ex sit natus quam unde et ea.", new DateTime(2019, 11, 22, 22, 27, 30, 235, DateTimeKind.Unspecified).AddTicks(2133), "Buckinghamshire", 17, 3, 3 },
                    { 7, new DateTime(2018, 7, 1, 13, 53, 38, 156, DateTimeKind.Unspecified).AddTicks(3223), "Suscipit incidunt numquam impedit est enim voluptates saepe odit.", new DateTime(2019, 12, 16, 23, 45, 33, 455, DateTimeKind.Unspecified).AddTicks(7552), "deposit", 17, 5, 3 },
                    { 3, new DateTime(2017, 7, 1, 15, 29, 53, 964, DateTimeKind.Unspecified).AddTicks(2644), "Dolores fugit qui facere nostrum voluptas corrupti molestiae magni sed quo.", new DateTime(2020, 4, 26, 7, 52, 11, 770, DateTimeKind.Unspecified).AddTicks(2745), "Kwacha", 17, 6, 3 },
                    { 25, new DateTime(2018, 11, 2, 22, 33, 56, 990, DateTimeKind.Unspecified).AddTicks(9069), "Et perspiciatis rerum velit adipisci perferendis tenetur cum quibusdam repudiandae consequuntur ut suscipit laborum.", new DateTime(2018, 2, 5, 15, 40, 3, 994, DateTimeKind.Unspecified).AddTicks(431), "Producer", 16, 5, 4 },
                    { 15, new DateTime(2017, 7, 20, 18, 58, 9, 175, DateTimeKind.Unspecified).AddTicks(3932), "Ex et nostrum voluptates reprehenderit et voluptates enim voluptas minima aut commodi.", new DateTime(2019, 8, 22, 21, 14, 2, 461, DateTimeKind.Unspecified).AddTicks(6646), "National", 16, 8, 3 },
                    { 13, new DateTime(2018, 7, 11, 4, 11, 26, 872, DateTimeKind.Unspecified).AddTicks(8320), "Distinctio voluptas facere quae non culpa harum eius impedit temporibus qui pariatur.", new DateTime(2019, 11, 7, 13, 26, 23, 883, DateTimeKind.Unspecified).AddTicks(5018), "Generic", 16, 5, 3 },
                    { 45, new DateTime(2017, 2, 14, 9, 6, 42, 518, DateTimeKind.Unspecified).AddTicks(6125), "Reiciendis sint cupiditate quasi natus sit libero sit quo.", new DateTime(2018, 9, 6, 12, 37, 56, 938, DateTimeKind.Unspecified).AddTicks(3048), "hacking", 15, 1, 2 },
                    { 18, new DateTime(2018, 4, 17, 18, 47, 37, 914, DateTimeKind.Unspecified).AddTicks(8056), "Corrupti vel porro provident ipsam aut rem repellat eveniet praesentium vel qui qui id.", new DateTime(2019, 9, 14, 11, 49, 25, 87, DateTimeKind.Unspecified).AddTicks(9772), "magenta", 14, 4, 2 },
                    { 16, new DateTime(2017, 1, 11, 17, 12, 36, 210, DateTimeKind.Unspecified).AddTicks(7464), "Ut pariatur rerum impedit reiciendis quibusdam et iusto.", new DateTime(2020, 1, 3, 11, 5, 14, 662, DateTimeKind.Unspecified).AddTicks(6189), "Plains", 14, 10, 1 },
                    { 50, new DateTime(2017, 6, 10, 14, 14, 40, 9, DateTimeKind.Unspecified).AddTicks(9492), "Est repudiandae facilis doloremque officia tempore ipsa sint sunt labore magnam corporis velit est.", new DateTime(2019, 12, 24, 2, 28, 9, 256, DateTimeKind.Unspecified).AddTicks(7583), "primary", 13, 4, 3 },
                    { 6, new DateTime(2017, 6, 2, 19, 31, 58, 557, DateTimeKind.Unspecified).AddTicks(1044), "Eveniet dolores ipsa et rerum minima quo incidunt odit aut ut sunt modi quis.", new DateTime(2017, 11, 30, 0, 15, 35, 918, DateTimeKind.Unspecified).AddTicks(2980), "Latvia", 12, 2, 2 },
                    { 19, new DateTime(2017, 8, 20, 4, 3, 56, 961, DateTimeKind.Unspecified).AddTicks(5727), "Et aliquam repellat quos eum.", new DateTime(2020, 3, 16, 13, 11, 20, 317, DateTimeKind.Unspecified).AddTicks(8993), "auxiliary", 11, 8, 1 },
                    { 1, new DateTime(2018, 9, 30, 3, 11, 48, 605, DateTimeKind.Unspecified).AddTicks(937), "Quo omnis eos aspernatur maxime adipisci a laboriosam laboriosam perspiciatis corrupti.", new DateTime(2020, 1, 6, 0, 7, 29, 589, DateTimeKind.Unspecified).AddTicks(230), "Refined", 24, 5, 1 },
                    { 10, new DateTime(2018, 5, 13, 15, 40, 1, 862, DateTimeKind.Unspecified).AddTicks(7176), "Vitae quasi quia a sed optio maiores nulla.", new DateTime(2019, 8, 20, 12, 38, 31, 354, DateTimeKind.Unspecified).AddTicks(6024), "Steel", 24, 1, 1 },
                    { 21, new DateTime(2018, 10, 23, 21, 51, 54, 813, DateTimeKind.Unspecified).AddTicks(4777), "Aut omnis vel officiis iste porro ipsa.", new DateTime(2019, 10, 13, 8, 29, 42, 415, DateTimeKind.Unspecified).AddTicks(6887), "array", 24, 5, 3 },
                    { 44, new DateTime(2018, 7, 9, 7, 50, 37, 232, DateTimeKind.Unspecified).AddTicks(9724), "Exercitationem quaerat est praesentium beatae dolorem nobis omnis.", new DateTime(2019, 5, 30, 4, 15, 18, 111, DateTimeKind.Unspecified).AddTicks(5860), "standardization", 25, 7, 3 },
                    { 29, new DateTime(2018, 8, 6, 22, 14, 39, 636, DateTimeKind.Unspecified).AddTicks(7333), "Itaque eaque vel qui id quo nostrum et inventore.", new DateTime(2019, 12, 26, 22, 25, 52, 279, DateTimeKind.Unspecified).AddTicks(8144), "Gorgeous Concrete Car", 10, 1, 4 },
                    { 17, new DateTime(2017, 1, 12, 10, 52, 16, 901, DateTimeKind.Unspecified).AddTicks(5176), "Voluptatibus temporibus laborum aut quia aut dolorum deserunt rerum eius deserunt rem.", new DateTime(2018, 11, 18, 9, 17, 54, 847, DateTimeKind.Unspecified).AddTicks(767), "Division", 9, 7, 3 },
                    { 49, new DateTime(2017, 5, 1, 21, 3, 36, 945, DateTimeKind.Unspecified).AddTicks(4689), "Ad ducimus quis voluptate dicta qui ab aut sint eum vitae.", new DateTime(2017, 5, 15, 22, 22, 24, 350, DateTimeKind.Unspecified).AddTicks(7515), "sexy", 9, 2, 4 },
                    { 42, new DateTime(2017, 11, 6, 23, 29, 26, 406, DateTimeKind.Unspecified).AddTicks(6455), "Aspernatur minima voluptatem vel accusantium.", new DateTime(2019, 1, 6, 0, 57, 22, 11, DateTimeKind.Unspecified).AddTicks(1545), "Handcrafted", 8, 1, 2 },
                    { 24, new DateTime(2017, 7, 13, 10, 2, 15, 76, DateTimeKind.Unspecified).AddTicks(1567), "Dolorem perferendis rerum voluptas excepturi cumque perferendis eos quos.", new DateTime(2020, 2, 21, 1, 57, 55, 406, DateTimeKind.Unspecified).AddTicks(4501), "Optional", 7, 4, 1 },
                    { 35, new DateTime(2018, 10, 10, 8, 30, 22, 868, DateTimeKind.Unspecified).AddTicks(3540), "Illum maxime officia doloremque dolores ut consequatur occaecati nisi voluptas impedit aut blanditiis.", new DateTime(2017, 4, 6, 1, 43, 17, 188, DateTimeKind.Unspecified).AddTicks(1604), "interfaces", 7, 7, 2 },
                    { 20, new DateTime(2017, 10, 10, 23, 29, 28, 351, DateTimeKind.Unspecified).AddTicks(4750), "Aut dolores aut ut quaerat ullam doloribus minus odit quisquam magni dolorem.", new DateTime(2020, 1, 21, 23, 11, 45, 884, DateTimeKind.Unspecified).AddTicks(3886), "Forward", 6, 6, 1 },
                    { 12, new DateTime(2018, 5, 22, 22, 35, 34, 119, DateTimeKind.Unspecified).AddTicks(6498), "Ea qui officiis ut laborum qui cumque qui laborum veritatis illum.", new DateTime(2019, 1, 2, 15, 24, 14, 156, DateTimeKind.Unspecified).AddTicks(8128), "ubiquitous", 6, 5, 2 },
                    { 23, new DateTime(2017, 12, 22, 6, 11, 29, 351, DateTimeKind.Unspecified).AddTicks(4513), "Occaecati officia omnis eos ea dolores in sunt.", new DateTime(2017, 12, 9, 8, 42, 52, 680, DateTimeKind.Unspecified).AddTicks(4356), "Human", 4, 2, 1 },
                    { 8, new DateTime(2017, 12, 1, 19, 38, 44, 627, DateTimeKind.Unspecified).AddTicks(5635), "Voluptatum fugit ea quo occaecati quos non.", new DateTime(2017, 10, 16, 9, 7, 29, 575, DateTimeKind.Unspecified).AddTicks(5861), "contingency", 4, 2, 1 },
                    { 14, new DateTime(2018, 7, 5, 12, 29, 31, 787, DateTimeKind.Unspecified).AddTicks(713), "Aut animi sed provident ut voluptate et id fugiat ullam ad.", new DateTime(2018, 1, 17, 17, 26, 42, 553, DateTimeKind.Unspecified).AddTicks(8832), "B2B", 11, 1, 4 },
                    { 22, new DateTime(2017, 9, 11, 13, 59, 48, 65, DateTimeKind.Unspecified).AddTicks(4406), "Optio pariatur animi eos dolores non nihil.", new DateTime(2017, 3, 7, 23, 18, 51, 333, DateTimeKind.Unspecified).AddTicks(8210), "California", 3, 3, 2 },
                    { 27, new DateTime(2018, 4, 18, 1, 37, 44, 87, DateTimeKind.Unspecified).AddTicks(5277), "Aut voluptatem inventore sapiente ipsa animi sunt.", new DateTime(2019, 11, 5, 12, 22, 46, 5, DateTimeKind.Unspecified).AddTicks(7067), "Small Plastic Bacon", 1, 3, 4 },
                    { 4, new DateTime(2017, 1, 3, 21, 24, 13, 29, DateTimeKind.Unspecified).AddTicks(5314), "Qui commodi harum aliquam numquam quos provident.", new DateTime(2019, 3, 23, 8, 12, 6, 861, DateTimeKind.Unspecified).AddTicks(110), "Home Loan Account", 1, 8, 2 },
                    { 37, new DateTime(2018, 9, 7, 8, 28, 48, 122, DateTimeKind.Unspecified).AddTicks(5794), "Omnis debitis ex molestiae qui illo accusamus est.", new DateTime(2017, 10, 25, 11, 45, 55, 435, DateTimeKind.Unspecified).AddTicks(8960), "Consultant", 1, 8, 3 },
                    { 36, new DateTime(2018, 1, 30, 13, 51, 44, 378, DateTimeKind.Unspecified).AddTicks(3837), "Sunt consectetur cumque eaque ut vero consectetur quia adipisci.", new DateTime(2018, 7, 11, 18, 46, 34, 902, DateTimeKind.Unspecified).AddTicks(8862), "Keys", 1, 7, 1 },
                    { 2, new DateTime(2018, 3, 24, 18, 29, 38, 838, DateTimeKind.Unspecified).AddTicks(3004), "Culpa consequatur error fugiat dolores.", new DateTime(2019, 11, 29, 20, 33, 13, 257, DateTimeKind.Unspecified).AddTicks(4998), "pricing structure", 29, 5, 3 },
                    { 41, new DateTime(2018, 8, 8, 17, 2, 17, 260, DateTimeKind.Unspecified).AddTicks(6533), "Ipsam nemo rerum vel temporibus.", new DateTime(2020, 7, 13, 1, 12, 16, 594, DateTimeKind.Unspecified).AddTicks(280), "Borders", 28, 9, 4 },
                    { 26, new DateTime(2018, 1, 9, 0, 40, 40, 498, DateTimeKind.Unspecified).AddTicks(8941), "Quis distinctio reprehenderit molestias qui veritatis et sint.", new DateTime(2019, 4, 27, 14, 26, 23, 171, DateTimeKind.Unspecified).AddTicks(781), "intuitive", 27, 9, 2 },
                    { 46, new DateTime(2017, 10, 31, 16, 14, 42, 800, DateTimeKind.Unspecified).AddTicks(535), "Sed voluptatem ea qui enim ratione similique impedit velit cumque quis nisi vel.", new DateTime(2020, 2, 27, 21, 15, 44, 690, DateTimeKind.Unspecified).AddTicks(16), "Business-focused", 27, 8, 2 },
                    { 40, new DateTime(2018, 9, 16, 3, 48, 50, 745, DateTimeKind.Unspecified).AddTicks(433), "Ipsum magni est alias vitae vitae assumenda ut minus vitae impedit quia aut a.", new DateTime(2020, 5, 3, 3, 3, 10, 688, DateTimeKind.Unspecified).AddTicks(3900), "Tasty Metal Hat", 27, 4, 1 },
                    { 31, new DateTime(2018, 6, 6, 1, 43, 23, 191, DateTimeKind.Unspecified).AddTicks(9851), "Excepturi ut possimus beatae iure sed aliquam qui debitis minus et et dolorem.", new DateTime(2017, 3, 19, 18, 14, 8, 412, DateTimeKind.Unspecified).AddTicks(9678), "Gorgeous Concrete Chips", 25, 8, 2 },
                    { 5, new DateTime(2018, 10, 11, 5, 2, 33, 449, DateTimeKind.Unspecified).AddTicks(789), "Placeat animi voluptate autem ipsam explicabo aut.", new DateTime(2018, 9, 1, 16, 27, 10, 695, DateTimeKind.Unspecified).AddTicks(1613), "Extended", 2, 7, 2 },
                    { 38, new DateTime(2017, 7, 14, 17, 48, 57, 551, DateTimeKind.Unspecified).AddTicks(2392), "Natus qui excepturi fugiat et ipsa omnis autem.", new DateTime(2017, 7, 22, 21, 37, 26, 358, DateTimeKind.Unspecified).AddTicks(3933), "web-readiness", 11, 6, 1 }
                });
        }
    }
}
