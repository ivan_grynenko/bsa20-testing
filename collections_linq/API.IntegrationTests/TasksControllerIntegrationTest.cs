﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace API.IntegrationTests
{
    public class TasksControllerIntegrationTest : IClassFixture<CustomeWebAppFactory<Startup>>
    {
        private readonly HttpClient client;

        public TasksControllerIntegrationTest(CustomeWebAppFactory<Startup> factory)
        {
            client = factory.CreateClient();
        }

        [Theory]
        [InlineData(1, HttpStatusCode.NoContent)]
        [InlineData(-1, HttpStatusCode.BadRequest)]
        [InlineData(51, HttpStatusCode.NotFound)]
        public async void Remove(int taskId, HttpStatusCode statusCode)
        {
            var httpResponse = await client.DeleteAsync($"api/tasks/{taskId}");

            Assert.Equal(statusCode, httpResponse.StatusCode);
        }
    }
}
