﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace API.IntegrationTests
{
    public class UsersControllerIntegrationTest : IClassFixture<CustomeWebAppFactory<Startup>>
    {
        private readonly HttpClient client;

        public UsersControllerIntegrationTest(CustomeWebAppFactory<Startup> factory)
        {
            client = factory.CreateClient();
        }

        [Theory]
        [InlineData(1, HttpStatusCode.NoContent)]
        [InlineData(-1, HttpStatusCode.BadRequest)]
        [InlineData(31, HttpStatusCode.NotFound)]
        public async void Remove(int userId, HttpStatusCode statusCode)
        {
            var httpResponse = await client.DeleteAsync($"api/users/{userId}");

            Assert.Equal(statusCode, httpResponse.StatusCode);
        }
    }
}
