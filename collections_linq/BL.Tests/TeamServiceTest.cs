﻿using BL.Interfaces;
using BL.Services;
using DAL.Base;
using DAL.Models;
using FakeItEasy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace BL.Tests
{
    public class TeamServiceTest
    {
        private ITeamService teamService;
        private IUnitOfWork unitOFWork;

        public TeamServiceTest()
        {
            unitOFWork = A.Fake<IUnitOfWork>();
            A.CallTo(() => unitOFWork.UserRepository.GetAll()).Returns(new List<User>()
            {
                new User()
                {
                    Id = 1,
                    FirstName = "Rick",
                    LastName = "Sanchez",
                    Email = "email@",
                    TeamId = 1,
                    Birthday = new DateTime(1940, 1, 1)
                },
                new User()
                {
                    Id = 2,
                    FirstName = "Morty",
                    LastName = "Smith",
                    Email = "email2@",
                    TeamId = 2,
                    Birthday = new DateTime(2010, 1, 1)
                }
            });
            A.CallTo(() => unitOFWork.TeamRepository.GetAll()).Returns(new List<Team>()
            {
                new Team()
                {
                    Id = 1,
                    Name = "Team A"
                },
                new Team()
                {
                    Id = 2,
                    Name = "Team B"
                }
            });
            teamService = new TeamService(unitOFWork);
        }

        [Theory]
        [InlineData(50, 1)]
        [InlineData(10, 2)]
        public async void GetTeamsOfCertainAge(int minAge, int expectedResult)
        {
            var result = await teamService.GetTeamsOfCertainAge(minAge);

            Assert.Equal(expectedResult, result.Count());
        }
    }
}
