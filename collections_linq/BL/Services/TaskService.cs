﻿using BL.Interfaces;
using DAL.Base;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.Services
{
    public class TaskService : ITaskService
    {
        private readonly IUnitOfWork unitOFWork;

        public TaskService(IUnitOfWork unitOFWork)
        {
            this.unitOFWork = unitOFWork;
        }

        public async Task<IEnumerable<Tasks>> GetAll()
        {
            return await unitOFWork.TaskRepository.GetAll();
        }

        public async Task<Tasks> GetById(int id)
        {
            return await unitOFWork.TaskRepository.GetById(id);
        }

        public async Task Add(IEnumerable<Tasks> entities)
        {
            await unitOFWork.TaskRepository.Add(entities);
            await unitOFWork.SaveChanges();
        }

        public async Task Add(Tasks entity)
        {
            await unitOFWork.TaskRepository.Add(entity);
            await unitOFWork.SaveChanges();
        }

        public async Task Remove(IEnumerable<Tasks> entities)
        {
            unitOFWork.TaskRepository.Remove(entities);
            await unitOFWork.SaveChanges();
        }

        public async Task Remove(Tasks entity)
        {
            unitOFWork.TaskRepository.Remove(entity);
            await unitOFWork.SaveChanges();
        }

        public async Task Update(IEnumerable<Tasks> entities)
        {
            //add check argument expt
            unitOFWork.TaskRepository.Update(entities);
            await unitOFWork.SaveChanges();
        }

        public async Task Update(Tasks entity)
        {
            unitOFWork.TaskRepository.Update(entity);
            await unitOFWork.SaveChanges();
        }

        public async Task<IDictionary<string, int>> GetNumberOfTasksByUser(int userId)
        {
            var tasks = await unitOFWork.TaskRepository.GetAll();
            var result = tasks.Where(e => e.PerformerId == userId)
                .GroupBy(e => e.ProjectId)
                .Select(e => new { ProjectId = e.Key, Count = e.Count() })
                .Join(await unitOFWork.ProjectRepository.GetAll(), t => t.ProjectId, p => p.Id, (t, p) => new { p.Name, t.Count })
                .ToDictionary(d => d.Name, d => d.Count);

            return result;
        }

        public async Task<IEnumerable<Tasks>> GetTasksByUserWithMaxNameLength(int userId, int maxLength)
        {
            var tasks = await unitOFWork.TaskRepository.GetAll();
            var result = tasks.Where(e => e.PerformerId == userId && e.Name.Length < maxLength);

            return result;
        }

        public async Task<IEnumerable<Tasks>> GetTasksByUserInCurrentYear(int userId)
        {
            var tasks = await unitOFWork.TaskRepository.GetAll();
            var result = tasks.Where(e => e.PerformerId == userId && e.FinishedAt?.Year == DateTime.Now.Year);

            return result;
        }

        public async Task<IEnumerable<Tasks>> GetUnfinishedTasksByUser(int userId)
        {
            var tasks = await unitOFWork.TaskRepository.GetAll();
            var result = tasks.Where(e => e.ProjectId == userId && e.StateId != 3);

            return result;
        }
    }
}
