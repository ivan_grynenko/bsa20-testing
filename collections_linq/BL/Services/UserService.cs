﻿using BL.Interfaces;
using BL.Models;
using DAL.Base;
using DAL.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork unitOFWork;

        public UserService(IUnitOfWork unitOFWork)
        {
            this.unitOFWork = unitOFWork;
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            return await unitOFWork.UserRepository.GetAll();
        }

        public async Task<User> GetById(int id)
        {
            return await unitOFWork.UserRepository.GetById(id);
        }

        public async Task Add(IEnumerable<User> entities)
        {
            await unitOFWork.UserRepository.Add(entities);
            await unitOFWork.SaveChanges();
        }

        public async Task Add(User entity)
        {
            await unitOFWork.UserRepository.Add(entity);
            await unitOFWork.SaveChanges();
        }

        public async Task Remove(IEnumerable<User> entities)
        {
            unitOFWork.UserRepository.Remove(entities);
            await unitOFWork.SaveChanges();
        }

        public async Task Remove(User entity)
        {
            unitOFWork.UserRepository.Remove(entity);
            await unitOFWork.SaveChanges();
        }

        public async Task Update(IEnumerable<User> entities)
        {
            unitOFWork.UserRepository.Update(entities);
            await unitOFWork.SaveChanges();
        }

        public async Task Update(User entity)
        {
            unitOFWork.UserRepository.Update(entity);
            await unitOFWork.SaveChanges();
        }

        public async Task<IEnumerable<UserTasks>> GetOrderedUsersWithOrderedTasks()
        {
            var users = await unitOFWork.UserRepository.GetAll();

            var result = users
                .Join(await unitOFWork.TaskRepository.GetAll(), u => u.Id, t => t.PerformerId, (u, t) => new { User = u, Tasks = t })
                .GroupBy(e => e.User)
                .Select(e => new UserTasks { User = e.Key, Tasks = e.Select(t => t.Tasks).OrderByDescending(t => t.Name.Length).AsEnumerable() })
                .OrderBy(e => e.User.FirstName)
                .AsEnumerable();

            return result;
        }

        public async Task<UserInfo> GetUserInfo(int userId)
        {
            var projects = await unitOFWork.ProjectRepository.GetAll();
            var tasks = await unitOFWork.TaskRepository.GetAll();
            var users = await unitOFWork.UserRepository.GetAll();

            var result = users.Where(e => e.Id == userId)
                .Join(tasks, u => u.Id, t => t.PerformerId, (u, t) => new { User = u, Task = t })
                .Join(projects, t => t.Task.ProjectId, p => p.Id, (t, p) => new { t.User, t.Task, Project = p })
                .GroupBy(e => e.User)
                .Select(e => new UserInfo
                {
                    User = e.Key,
                    LastProject = e.Select(p => p.Project).OrderByDescending(p => p.CreatedAt).FirstOrDefault(),
                    LongestTask = e.Select(t => t.Task).OrderBy(t => t.FinishedAt - t.CreatedAt).LastOrDefault(),
                    OverallNumOfCanceledTasks = e.Select(t => t.Task).Where(t => t.StateId == 2 || t.StateId == 4).Count(),
                    OverallNumOfTasks = e.Select(t => t.Task).Where(t => t.ProjectId == e.Select(p => p.Project).OrderByDescending(p => p.CreatedAt).FirstOrDefault().Id).Count()
                }).FirstOrDefault();

            return result;
        }
    }
}
