﻿using DAL.Models;
using System;

namespace BL.Models
{
    public class ProjectInfo
    {
        public Project Project { get; set; }
        public Tasks LongestTask { get; set; }
        public Tasks ShortestTask { get; set; }
    }
}
