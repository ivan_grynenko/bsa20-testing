﻿using System;

namespace API.Models
{
    public enum TaskStates
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}
