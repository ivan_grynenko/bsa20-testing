﻿using DAL.Models;
using System;

namespace BL.Models
{
    public class UserInfo
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int? OverallNumOfTasks { get; set; }
        public int? OverallNumOfCanceledTasks { get; set; }
        public Tasks LongestTask { get; set; }
    }
}
