﻿using BL.Models;
using DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BL.Interfaces
{
    public interface IUserService : IService<User>
    {
        Task<IEnumerable<UserTasks>> GetOrderedUsersWithOrderedTasks();
        Task<UserInfo> GetUserInfo(int userId);
    }
}